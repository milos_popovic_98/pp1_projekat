
class A {
    int a = 1;
    public void m1(int a){
        System.out.println("m1 - a");
    }

}
class B extends A{
    int a = 2;
    public void m1() {

    }
}
class C {
    public void m2(A a) {
        System.out.println("m2 - " + a.a);
    }
}
public class JavaTemp {
    public static void main(String[] args) {
        int x = 3;
        do {
            System.out.println("u petlji");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (1 == 1) continue;
            System.out.println("u petlji 2");
        } while(x > 5);
    }
}
