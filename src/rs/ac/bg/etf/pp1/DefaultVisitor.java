package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;
import rs.ac.bg.etf.pp1.ast.YieldStatement;

public class DefaultVisitor extends VisitorAdaptor {
    private boolean yieldFound;
    private SemanticAnalyzer semanticAnalyzer;

    public DefaultVisitor(SemanticAnalyzer semanticAnalyzer) {
        this.semanticAnalyzer = semanticAnalyzer;
    }

    public void visit(YieldStatement statement) {
        yieldFound = true;
    }

    public boolean getStatus() {
        return yieldFound;
    }
}
