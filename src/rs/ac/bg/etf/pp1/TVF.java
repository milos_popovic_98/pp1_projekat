package rs.ac.bg.etf.pp1;

import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

import java.util.Collection;
import java.util.HashMap;

public class TVF {
    private HashMap<Struct, Integer>  tableVectors = new HashMap<>();
    private static TVF instance;
    private static int METHOD_NAME_END = -1;
    private static int CLASS_TVF_END = -2;
    private int currentAdr;
    public static TVF getTVF() {
        if (instance == null) {
            instance = new TVF();
        }
        return instance;
    }

    public void fillTable(String progName) {
        Obj obj = Tab.find(progName);
        if (obj != null) {
            Collection<Obj> members = obj.getLocalSymbols();
            for(Obj member : members) {
                if (member.getType().getKind() == Struct.Class) {
                    createTable(member.getType());
                    addToStaticArea(CLASS_TVF_END);
                }
            }
            Code.dataSize = currentAdr;
        }
    }

    protected void createTable(Struct classStruct) {
        tableVectors.put(classStruct, currentAdr);
        Collection<Obj> members = classStruct.getMembers();
        for(Obj member : members) {
            if (member.getKind() == Obj.Meth) {
                addMethodToTable(member);
            }
        }
    }

    protected void addMethodToTable(Obj member) {
        String methName = member.getName();
        for(int i = 0; i < methName.length(); i++){
            char c = methName.charAt(i);
            addToStaticArea(c);
        }
        addToStaticArea(METHOD_NAME_END);
        addToStaticArea(member.getAdr());
    }

    protected void addToStaticArea(int n){
        Code.loadConst(n);
        Code.put(Code.putstatic);
        Code.put2(currentAdr);
        currentAdr++;
    }

    public int getTvfStartAdr(Struct className) {
        return tableVectors.get(className);
    }

    public void setNumberOfGlobalVariables(int numberOfGlobalVariables) {
        this.currentAdr = numberOfGlobalVariables;
    }


}
