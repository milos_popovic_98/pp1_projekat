package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.Expr;
import rs.ac.bg.etf.pp1.ast.Expr1;

import java.util.ArrayList;
import java.util.List;

public class SwitchContext extends BranchContext {
    private List<Integer> branchesAfter = new ArrayList<>();
    private List<Integer> branchesOn = new ArrayList<>();
    private List<Integer> thenBranches = new ArrayList<>();
    private Expr1 expr;
    private boolean firstCase = true;

    @Override
    public void addBranchOn(int adr) {
        branchesOn.add(adr);
    }

    @Override
    public void addBranchAfter(int adr) {
        branchesAfter.add(adr);
    }

    @Override
    public void addBranchThen(int adr) {
        thenBranches.add(adr);
    }

    @Override
    public List<Integer> getBranchesOn() {
        return branchesOn;
    }

    @Override
    public List<Integer> getBranchesAfter() {
        return branchesAfter;
    }

    @Override
    public List<Integer> getBranchesThen() {
        return thenBranches;
    }

    public void setExpr(Expr1 expr) {
        this.expr = expr;
    }

    public Expr1 getExpr() {
        return expr;
    }

    public boolean isFirstCase() {
        return firstCase;
    }

    public void setFirstCase(boolean firstCase) {
        this.firstCase = firstCase;
    }
}
