package rs.ac.bg.etf.pp1;

import rs.etf.pp1.symboltable.concepts.Obj;

import java.util.List;

public class Context {
    private CodeGenerator.State state;
    private Obj previousObjInChain;
    private boolean isArrayIndexed;

    public Context(CodeGenerator.State state) {
        this.state = state;
    }

    public Context(CodeGenerator.State state, Obj previousObjInChain, boolean isArrayIndexed) {
        this.state = state;
        this.previousObjInChain = previousObjInChain;
        this.isArrayIndexed = isArrayIndexed;
    }

    public CodeGenerator.State getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Obj getPreviousObjInChain() {
        return previousObjInChain;
    }

    public void setPreviousObjInChain(Obj previousObjInChain) {
        this.previousObjInChain = previousObjInChain;
    }

    public boolean getIsArrayIndexed() {
        return isArrayIndexed;
    }

    public void setIsArrayIndexed(boolean isArrayIndexed) {
        this.isArrayIndexed = isArrayIndexed;
    }

}
