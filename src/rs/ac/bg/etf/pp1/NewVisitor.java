package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.ExprInSquares;
import rs.ac.bg.etf.pp1.ast.FactorNew;
import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;
import rs.etf.pp1.symboltable.concepts.Struct;

public class NewVisitor extends VisitorAdaptor {
    private boolean newFound;
    private Struct type;
    public void visit(FactorNew factorNew) {
        if (!(factorNew.getOptExprInSquares() instanceof ExprInSquares)) {
            newFound = true;
            type = factorNew.struct;
        }
    }

    public boolean getStatus() {
        return newFound;
    }

    public Struct getType() {
        return type;
    }
}
