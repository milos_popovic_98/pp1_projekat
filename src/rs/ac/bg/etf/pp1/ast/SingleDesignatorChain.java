// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class SingleDesignatorChain extends DesignatorChain {

    private DesignatorVal DesignatorVal;

    public SingleDesignatorChain (DesignatorVal DesignatorVal) {
        this.DesignatorVal=DesignatorVal;
        if(DesignatorVal!=null) DesignatorVal.setParent(this);
    }

    public DesignatorVal getDesignatorVal() {
        return DesignatorVal;
    }

    public void setDesignatorVal(DesignatorVal DesignatorVal) {
        this.DesignatorVal=DesignatorVal;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorVal!=null) DesignatorVal.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorVal!=null) DesignatorVal.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorVal!=null) DesignatorVal.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("SingleDesignatorChain(\n");

        if(DesignatorVal!=null)
            buffer.append(DesignatorVal.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [SingleDesignatorChain]");
        return buffer.toString();
    }
}
