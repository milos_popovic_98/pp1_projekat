// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class ExprSum extends Expr {

    private Sum Sum;

    public ExprSum (Sum Sum) {
        this.Sum=Sum;
        if(Sum!=null) Sum.setParent(this);
    }

    public Sum getSum() {
        return Sum;
    }

    public void setSum(Sum Sum) {
        this.Sum=Sum;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Sum!=null) Sum.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Sum!=null) Sum.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Sum!=null) Sum.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ExprSum(\n");

        if(Sum!=null)
            buffer.append(Sum.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ExprSum]");
        return buffer.toString();
    }
}
