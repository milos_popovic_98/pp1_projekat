// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class SwitchYieldExpr extends Expr {

    private SwitchStart SwitchStart;
    private SwitchExprStart SwitchExprStart;
    private Expr1 Expr1;
    private SwitchExprEnd SwitchExprEnd;
    private CaseList CaseList;
    private DefaultStatement DefaultStatement;

    public SwitchYieldExpr (SwitchStart SwitchStart, SwitchExprStart SwitchExprStart, Expr1 Expr1, SwitchExprEnd SwitchExprEnd, CaseList CaseList, DefaultStatement DefaultStatement) {
        this.SwitchStart=SwitchStart;
        if(SwitchStart!=null) SwitchStart.setParent(this);
        this.SwitchExprStart=SwitchExprStart;
        if(SwitchExprStart!=null) SwitchExprStart.setParent(this);
        this.Expr1=Expr1;
        if(Expr1!=null) Expr1.setParent(this);
        this.SwitchExprEnd=SwitchExprEnd;
        if(SwitchExprEnd!=null) SwitchExprEnd.setParent(this);
        this.CaseList=CaseList;
        if(CaseList!=null) CaseList.setParent(this);
        this.DefaultStatement=DefaultStatement;
        if(DefaultStatement!=null) DefaultStatement.setParent(this);
    }

    public SwitchStart getSwitchStart() {
        return SwitchStart;
    }

    public void setSwitchStart(SwitchStart SwitchStart) {
        this.SwitchStart=SwitchStart;
    }

    public SwitchExprStart getSwitchExprStart() {
        return SwitchExprStart;
    }

    public void setSwitchExprStart(SwitchExprStart SwitchExprStart) {
        this.SwitchExprStart=SwitchExprStart;
    }

    public Expr1 getExpr1() {
        return Expr1;
    }

    public void setExpr1(Expr1 Expr1) {
        this.Expr1=Expr1;
    }

    public SwitchExprEnd getSwitchExprEnd() {
        return SwitchExprEnd;
    }

    public void setSwitchExprEnd(SwitchExprEnd SwitchExprEnd) {
        this.SwitchExprEnd=SwitchExprEnd;
    }

    public CaseList getCaseList() {
        return CaseList;
    }

    public void setCaseList(CaseList CaseList) {
        this.CaseList=CaseList;
    }

    public DefaultStatement getDefaultStatement() {
        return DefaultStatement;
    }

    public void setDefaultStatement(DefaultStatement DefaultStatement) {
        this.DefaultStatement=DefaultStatement;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(SwitchStart!=null) SwitchStart.accept(visitor);
        if(SwitchExprStart!=null) SwitchExprStart.accept(visitor);
        if(Expr1!=null) Expr1.accept(visitor);
        if(SwitchExprEnd!=null) SwitchExprEnd.accept(visitor);
        if(CaseList!=null) CaseList.accept(visitor);
        if(DefaultStatement!=null) DefaultStatement.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(SwitchStart!=null) SwitchStart.traverseTopDown(visitor);
        if(SwitchExprStart!=null) SwitchExprStart.traverseTopDown(visitor);
        if(Expr1!=null) Expr1.traverseTopDown(visitor);
        if(SwitchExprEnd!=null) SwitchExprEnd.traverseTopDown(visitor);
        if(CaseList!=null) CaseList.traverseTopDown(visitor);
        if(DefaultStatement!=null) DefaultStatement.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(SwitchStart!=null) SwitchStart.traverseBottomUp(visitor);
        if(SwitchExprStart!=null) SwitchExprStart.traverseBottomUp(visitor);
        if(Expr1!=null) Expr1.traverseBottomUp(visitor);
        if(SwitchExprEnd!=null) SwitchExprEnd.traverseBottomUp(visitor);
        if(CaseList!=null) CaseList.traverseBottomUp(visitor);
        if(DefaultStatement!=null) DefaultStatement.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("SwitchYieldExpr(\n");

        if(SwitchStart!=null)
            buffer.append(SwitchStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(SwitchExprStart!=null)
            buffer.append(SwitchExprStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr1!=null)
            buffer.append(Expr1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(SwitchExprEnd!=null)
            buffer.append(SwitchExprEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(CaseList!=null)
            buffer.append(CaseList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DefaultStatement!=null)
            buffer.append(DefaultStatement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [SwitchYieldExpr]");
        return buffer.toString();
    }
}
