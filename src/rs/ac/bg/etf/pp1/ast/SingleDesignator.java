// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class SingleDesignator extends Designator {

    private DesignatorFirst DesignatorFirst;
    private OptDesignatorValArray OptDesignatorValArray;

    public SingleDesignator (DesignatorFirst DesignatorFirst, OptDesignatorValArray OptDesignatorValArray) {
        this.DesignatorFirst=DesignatorFirst;
        if(DesignatorFirst!=null) DesignatorFirst.setParent(this);
        this.OptDesignatorValArray=OptDesignatorValArray;
        if(OptDesignatorValArray!=null) OptDesignatorValArray.setParent(this);
    }

    public DesignatorFirst getDesignatorFirst() {
        return DesignatorFirst;
    }

    public void setDesignatorFirst(DesignatorFirst DesignatorFirst) {
        this.DesignatorFirst=DesignatorFirst;
    }

    public OptDesignatorValArray getOptDesignatorValArray() {
        return OptDesignatorValArray;
    }

    public void setOptDesignatorValArray(OptDesignatorValArray OptDesignatorValArray) {
        this.OptDesignatorValArray=OptDesignatorValArray;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorFirst!=null) DesignatorFirst.accept(visitor);
        if(OptDesignatorValArray!=null) OptDesignatorValArray.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorFirst!=null) DesignatorFirst.traverseTopDown(visitor);
        if(OptDesignatorValArray!=null) OptDesignatorValArray.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorFirst!=null) DesignatorFirst.traverseBottomUp(visitor);
        if(OptDesignatorValArray!=null) OptDesignatorValArray.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("SingleDesignator(\n");

        if(DesignatorFirst!=null)
            buffer.append(DesignatorFirst.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptDesignatorValArray!=null)
            buffer.append(OptDesignatorValArray.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [SingleDesignator]");
        return buffer.toString();
    }
}
