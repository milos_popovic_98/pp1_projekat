// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class StatementDerived6 extends Statement {

    private BreakStatement BreakStatement;

    public StatementDerived6 (BreakStatement BreakStatement) {
        this.BreakStatement=BreakStatement;
        if(BreakStatement!=null) BreakStatement.setParent(this);
    }

    public BreakStatement getBreakStatement() {
        return BreakStatement;
    }

    public void setBreakStatement(BreakStatement BreakStatement) {
        this.BreakStatement=BreakStatement;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(BreakStatement!=null) BreakStatement.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(BreakStatement!=null) BreakStatement.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(BreakStatement!=null) BreakStatement.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("StatementDerived6(\n");

        if(BreakStatement!=null)
            buffer.append(BreakStatement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [StatementDerived6]");
        return buffer.toString();
    }
}
