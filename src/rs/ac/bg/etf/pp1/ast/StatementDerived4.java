// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class StatementDerived4 extends Statement {

    private YieldStatement YieldStatement;

    public StatementDerived4 (YieldStatement YieldStatement) {
        this.YieldStatement=YieldStatement;
        if(YieldStatement!=null) YieldStatement.setParent(this);
    }

    public YieldStatement getYieldStatement() {
        return YieldStatement;
    }

    public void setYieldStatement(YieldStatement YieldStatement) {
        this.YieldStatement=YieldStatement;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(YieldStatement!=null) YieldStatement.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(YieldStatement!=null) YieldStatement.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(YieldStatement!=null) YieldStatement.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("StatementDerived4(\n");

        if(YieldStatement!=null)
            buffer.append(YieldStatement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [StatementDerived4]");
        return buffer.toString();
    }
}
