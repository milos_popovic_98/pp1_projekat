// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(OptMethodVarDecls OptMethodVarDecls) { }
    public void visit(FormPars FormPars) { }
    public void visit(Factor Factor) { }
    public void visit(Statement Statement) { }
    public void visit(Vars Vars) { }
    public void visit(OptMethodDecls OptMethodDecls) { }
    public void visit(VarDecls VarDecls) { }
    public void visit(Relop Relop) { }
    public void visit(DeclList DeclList) { }
    public void visit(OptMinus OptMinus) { }
    public void visit(CaseList CaseList) { }
    public void visit(Expr Expr) { }
    public void visit(DesignatorChain DesignatorChain) { }
    public void visit(OptExtends OptExtends) { }
    public void visit(OptActPars OptActPars) { }
    public void visit(OptFieldDecls OptFieldDecls) { }
    public void visit(DeclLists DeclLists) { }
    public void visit(Condition Condition) { }
    public void visit(Mulop Mulop) { }
    public void visit(OptActParsInBrackets OptActParsInBrackets) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(OptExprInSquares OptExprInSquares) { }
    public void visit(Addop Addop) { }
    public void visit(StatementList StatementList) { }
    public void visit(OptDesignatorValArray OptDesignatorValArray) { }
    public void visit(CondTerm CondTerm) { }
    public void visit(IfStatement IfStatement) { }
    public void visit(ConstVars ConstVars) { }
    public void visit(ClassDecl ClassDecl) { }
    public void visit(MethodDeclList MethodDeclList) { }
    public void visit(OptFormPars OptFormPars) { }
    public void visit(MethodType MethodType) { }
    public void visit(OptArray OptArray) { }
    public void visit(ConstValue ConstValue) { }
    public void visit(OptNumConst OptNumConst) { }
    public void visit(Sum Sum) { }
    public void visit(Expr1 Expr1) { }
    public void visit(FormParam FormParam) { }
    public void visit(ActPars ActPars) { }
    public void visit(Designator Designator) { }
    public void visit(CondFact CondFact) { }
    public void visit(Operation Operation) { }
    public void visit(Term Term) { }
    public void visit(Var Var) { }
    public void visit(NoExprInSquares NoExprInSquares) { visit(); }
    public void visit(ExprInSquares ExprInSquares) { visit(); }
    public void visit(NoActParsInBrackets NoActParsInBrackets) { visit(); }
    public void visit(ActParsInBrackets ActParsInBrackets) { visit(); }
    public void visit(FactorExpr FactorExpr) { visit(); }
    public void visit(FactorNew FactorNew) { visit(); }
    public void visit(FactorConst FactorConst) { visit(); }
    public void visit(FactorDesignator FactorDesignator) { visit(); }
    public void visit(FactorTerm FactorTerm) { visit(); }
    public void visit(MulTerm MulTerm) { visit(); }
    public void visit(TermImpl TermImpl) { visit(); }
    public void visit(SumImpl SumImpl) { visit(); }
    public void visit(NoMinusBegin NoMinusBegin) { visit(); }
    public void visit(MinusBegin MinusBegin) { visit(); }
    public void visit(DefaultValue DefaultValue) { visit(); }
    public void visit(DefaultStatement DefaultStatement) { visit(); }
    public void visit(SwitchYieldExpr SwitchYieldExpr) { visit(); }
    public void visit(ExprSum ExprSum) { visit(); }
    public void visit(ColonValue ColonValue) { visit(); }
    public void visit(QuestionValue QuestionValue) { visit(); }
    public void visit(ExprImpl ExprImpl) { visit(); }
    public void visit(TernaryOperator TernaryOperator) { visit(); }
    public void visit(EndOfDesignatorIndex EndOfDesignatorIndex) { visit(); }
    public void visit(StartOfDesignatorIndex StartOfDesignatorIndex) { visit(); }
    public void visit(NoDesignatorValArray NoDesignatorValArray) { visit(); }
    public void visit(DesignatorValArray DesignatorValArray) { visit(); }
    public void visit(DesignatorValName DesignatorValName) { visit(); }
    public void visit(DesignatorVal DesignatorVal) { visit(); }
    public void visit(SingleDesignatorChain SingleDesignatorChain) { visit(); }
    public void visit(DesignatorChainImpl DesignatorChainImpl) { visit(); }
    public void visit(DesignatorFirstArr DesignatorFirstArr) { visit(); }
    public void visit(DesignatorFirst DesignatorFirst) { visit(); }
    public void visit(SingleDesignator SingleDesignator) { visit(); }
    public void visit(DesignatorArr DesignatorArr) { visit(); }
    public void visit(DesignatorDot DesignatorDot) { visit(); }
    public void visit(ActParam ActParam) { visit(); }
    public void visit(SingleActParam SingleActParam) { visit(); }
    public void visit(ActParamList ActParamList) { visit(); }
    public void visit(NoActPars NoActPars) { visit(); }
    public void visit(ActParsList ActParsList) { visit(); }
    public void visit(Mod Mod) { visit(); }
    public void visit(Div Div) { visit(); }
    public void visit(Mul Mul) { visit(); }
    public void visit(Minus Minus) { visit(); }
    public void visit(Plus Plus) { visit(); }
    public void visit(RelopLt RelopLt) { visit(); }
    public void visit(RelopLe RelopLe) { visit(); }
    public void visit(RelopGt RelopGt) { visit(); }
    public void visit(RelopGe RelopGe) { visit(); }
    public void visit(RelopNeq RelopNeq) { visit(); }
    public void visit(RelopEq RelopEq) { visit(); }
    public void visit(Assignop Assignop) { visit(); }
    public void visit(AndCond AndCond) { visit(); }
    public void visit(OrCond OrCond) { visit(); }
    public void visit(CondFactExpr CondFactExpr) { visit(); }
    public void visit(RelopCondFact RelopCondFact) { visit(); }
    public void visit(SimpleCondTerm SimpleCondTerm) { visit(); }
    public void visit(AndConditionTerm AndConditionTerm) { visit(); }
    public void visit(ErrorCondition ErrorCondition) { visit(); }
    public void visit(SimpleCondition SimpleCondition) { visit(); }
    public void visit(OrCondtion OrCondtion) { visit(); }
    public void visit(ActParsEnd ActParsEnd) { visit(); }
    public void visit(ActParsStart ActParsStart) { visit(); }
    public void visit(Decrement Decrement) { visit(); }
    public void visit(Increment Increment) { visit(); }
    public void visit(FunctionCall FunctionCall) { visit(); }
    public void visit(AssignOperation AssignOperation) { visit(); }
    public void visit(ErrorAssign ErrorAssign) { visit(); }
    public void visit(DesignatorStatement1 DesignatorStatement1) { visit(); }
    public void visit(NoNumConst NoNumConst) { visit(); }
    public void visit(NumConst NumConst) { visit(); }
    public void visit(PrintEnd PrintEnd) { visit(); }
    public void visit(PrintStart PrintStart) { visit(); }
    public void visit(PrintStatement PrintStatement) { visit(); }
    public void visit(ReadStatement ReadStatement) { visit(); }
    public void visit(ReturnStart ReturnStart) { visit(); }
    public void visit(ReturnStatement ReturnStatement) { visit(); }
    public void visit(ReturnStatementWithExpr ReturnStatementWithExpr) { visit(); }
    public void visit(ContinueStatement ContinueStatement) { visit(); }
    public void visit(BreakStatement BreakStatement) { visit(); }
    public void visit(CaseStart CaseStart) { visit(); }
    public void visit(CaseStatement CaseStatement) { visit(); }
    public void visit(NoCases NoCases) { visit(); }
    public void visit(CaseListImpl CaseListImpl) { visit(); }
    public void visit(SwitchExprEnd SwitchExprEnd) { visit(); }
    public void visit(SwitchExprStart SwitchExprStart) { visit(); }
    public void visit(SwitchStart SwitchStart) { visit(); }
    public void visit(SwitchStatement SwitchStatement) { visit(); }
    public void visit(YieldStatement YieldStatement) { visit(); }
    public void visit(WhileValue WhileValue) { visit(); }
    public void visit(DoValue DoValue) { visit(); }
    public void visit(DoWhileStatement DoWhileStatement) { visit(); }
    public void visit(ElseBranch ElseBranch) { visit(); }
    public void visit(ConditionEnd ConditionEnd) { visit(); }
    public void visit(ConditionStart ConditionStart) { visit(); }
    public void visit(IfStart IfStart) { visit(); }
    public void visit(IfThenStatement IfThenStatement) { visit(); }
    public void visit(IfElseStatement IfElseStatement) { visit(); }
    public void visit(StatementDerived11 StatementDerived11) { visit(); }
    public void visit(StatementDerived10 StatementDerived10) { visit(); }
    public void visit(StatementDerived9 StatementDerived9) { visit(); }
    public void visit(StatementDerived8 StatementDerived8) { visit(); }
    public void visit(StatementDerived7 StatementDerived7) { visit(); }
    public void visit(StatementDerived6 StatementDerived6) { visit(); }
    public void visit(StatementDerived5 StatementDerived5) { visit(); }
    public void visit(StatementDerived4 StatementDerived4) { visit(); }
    public void visit(StatementDerived3 StatementDerived3) { visit(); }
    public void visit(StatementDerived2 StatementDerived2) { visit(); }
    public void visit(StatementDerived1 StatementDerived1) { visit(); }
    public void visit(NoStatements NoStatements) { visit(); }
    public void visit(StatementListImpl StatementListImpl) { visit(); }
    public void visit(Scalar Scalar) { visit(); }
    public void visit(Array Array) { visit(); }
    public void visit(ErrorFormParam ErrorFormParam) { visit(); }
    public void visit(FormParam1 FormParam1) { visit(); }
    public void visit(SingleFormParam SingleFormParam) { visit(); }
    public void visit(FormParamDecls FormParamDecls) { visit(); }
    public void visit(NoFormParams NoFormParams) { visit(); }
    public void visit(FormParams FormParams) { visit(); }
    public void visit(MethodTypeVoid MethodTypeVoid) { visit(); }
    public void visit(MethodTypeImpl MethodTypeImpl) { visit(); }
    public void visit(MethodTypeName MethodTypeName) { visit(); }
    public void visit(MethodDecl MethodDecl) { visit(); }
    public void visit(NoMethodDeclarations NoMethodDeclarations) { visit(); }
    public void visit(MethodDeclarations MethodDeclarations) { visit(); }
    public void visit(NoClassMethods NoClassMethods) { visit(); }
    public void visit(ClassMethodDecls ClassMethodDecls) { visit(); }
    public void visit(NoMethodVars NoMethodVars) { visit(); }
    public void visit(MethodVars MethodVars) { visit(); }
    public void visit(NoFieldDecls NoFieldDecls) { visit(); }
    public void visit(FieldDecls FieldDecls) { visit(); }
    public void visit(ExtendsError ExtendsError) { visit(); }
    public void visit(NoExtends NoExtends) { visit(); }
    public void visit(Extends Extends) { visit(); }
    public void visit(Type Type) { visit(); }
    public void visit(ClassName ClassName) { visit(); }
    public void visit(ClassDecl1 ClassDecl1) { visit(); }
    public void visit(ErrorVar ErrorVar) { visit(); }
    public void visit(Var1 Var1) { visit(); }
    public void visit(SingleVar SingleVar) { visit(); }
    public void visit(VarVars VarVars) { visit(); }
    public void visit(VarDecls1 VarDecls1) { visit(); }
    public void visit(ConstValueBool ConstValueBool) { visit(); }
    public void visit(ConstValueChar ConstValueChar) { visit(); }
    public void visit(ConstValueNumber ConstValueNumber) { visit(); }
    public void visit(ConstVar ConstVar) { visit(); }
    public void visit(SingleConstVar SingleConstVar) { visit(); }
    public void visit(ConstVarsImpl ConstVarsImpl) { visit(); }
    public void visit(ConstType ConstType) { visit(); }
    public void visit(ConstDecls ConstDecls) { visit(); }
    public void visit(ClassDeclaration ClassDeclaration) { visit(); }
    public void visit(VarDeclarations VarDeclarations) { visit(); }
    public void visit(ConstDeclarations ConstDeclarations) { visit(); }
    public void visit(NoDeclarations NoDeclarations) { visit(); }
    public void visit(Declarations Declarations) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
