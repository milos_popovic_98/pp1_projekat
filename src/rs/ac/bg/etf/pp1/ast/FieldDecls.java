// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class FieldDecls extends OptFieldDecls {

    private OptFieldDecls OptFieldDecls;
    private VarDecls VarDecls;

    public FieldDecls (OptFieldDecls OptFieldDecls, VarDecls VarDecls) {
        this.OptFieldDecls=OptFieldDecls;
        if(OptFieldDecls!=null) OptFieldDecls.setParent(this);
        this.VarDecls=VarDecls;
        if(VarDecls!=null) VarDecls.setParent(this);
    }

    public OptFieldDecls getOptFieldDecls() {
        return OptFieldDecls;
    }

    public void setOptFieldDecls(OptFieldDecls OptFieldDecls) {
        this.OptFieldDecls=OptFieldDecls;
    }

    public VarDecls getVarDecls() {
        return VarDecls;
    }

    public void setVarDecls(VarDecls VarDecls) {
        this.VarDecls=VarDecls;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(OptFieldDecls!=null) OptFieldDecls.accept(visitor);
        if(VarDecls!=null) VarDecls.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(OptFieldDecls!=null) OptFieldDecls.traverseTopDown(visitor);
        if(VarDecls!=null) VarDecls.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(OptFieldDecls!=null) OptFieldDecls.traverseBottomUp(visitor);
        if(VarDecls!=null) VarDecls.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("FieldDecls(\n");

        if(OptFieldDecls!=null)
            buffer.append(OptFieldDecls.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDecls!=null)
            buffer.append(VarDecls.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [FieldDecls]");
        return buffer.toString();
    }
}
