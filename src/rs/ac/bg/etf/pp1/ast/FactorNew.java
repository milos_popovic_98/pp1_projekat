// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class FactorNew extends Factor {

    private Type Type;
    private OptExprInSquares OptExprInSquares;

    public FactorNew (Type Type, OptExprInSquares OptExprInSquares) {
        this.Type=Type;
        if(Type!=null) Type.setParent(this);
        this.OptExprInSquares=OptExprInSquares;
        if(OptExprInSquares!=null) OptExprInSquares.setParent(this);
    }

    public Type getType() {
        return Type;
    }

    public void setType(Type Type) {
        this.Type=Type;
    }

    public OptExprInSquares getOptExprInSquares() {
        return OptExprInSquares;
    }

    public void setOptExprInSquares(OptExprInSquares OptExprInSquares) {
        this.OptExprInSquares=OptExprInSquares;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Type!=null) Type.accept(visitor);
        if(OptExprInSquares!=null) OptExprInSquares.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Type!=null) Type.traverseTopDown(visitor);
        if(OptExprInSquares!=null) OptExprInSquares.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Type!=null) Type.traverseBottomUp(visitor);
        if(OptExprInSquares!=null) OptExprInSquares.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("FactorNew(\n");

        if(Type!=null)
            buffer.append(Type.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptExprInSquares!=null)
            buffer.append(OptExprInSquares.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [FactorNew]");
        return buffer.toString();
    }
}
