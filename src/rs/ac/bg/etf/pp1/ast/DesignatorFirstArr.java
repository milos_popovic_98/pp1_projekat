// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class DesignatorFirstArr implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private DesignatorFirst DesignatorFirst;
    private StartOfDesignatorIndex StartOfDesignatorIndex;
    private Expr1 Expr1;
    private EndOfDesignatorIndex EndOfDesignatorIndex;

    public DesignatorFirstArr (DesignatorFirst DesignatorFirst, StartOfDesignatorIndex StartOfDesignatorIndex, Expr1 Expr1, EndOfDesignatorIndex EndOfDesignatorIndex) {
        this.DesignatorFirst=DesignatorFirst;
        if(DesignatorFirst!=null) DesignatorFirst.setParent(this);
        this.StartOfDesignatorIndex=StartOfDesignatorIndex;
        if(StartOfDesignatorIndex!=null) StartOfDesignatorIndex.setParent(this);
        this.Expr1=Expr1;
        if(Expr1!=null) Expr1.setParent(this);
        this.EndOfDesignatorIndex=EndOfDesignatorIndex;
        if(EndOfDesignatorIndex!=null) EndOfDesignatorIndex.setParent(this);
    }

    public DesignatorFirst getDesignatorFirst() {
        return DesignatorFirst;
    }

    public void setDesignatorFirst(DesignatorFirst DesignatorFirst) {
        this.DesignatorFirst=DesignatorFirst;
    }

    public StartOfDesignatorIndex getStartOfDesignatorIndex() {
        return StartOfDesignatorIndex;
    }

    public void setStartOfDesignatorIndex(StartOfDesignatorIndex StartOfDesignatorIndex) {
        this.StartOfDesignatorIndex=StartOfDesignatorIndex;
    }

    public Expr1 getExpr1() {
        return Expr1;
    }

    public void setExpr1(Expr1 Expr1) {
        this.Expr1=Expr1;
    }

    public EndOfDesignatorIndex getEndOfDesignatorIndex() {
        return EndOfDesignatorIndex;
    }

    public void setEndOfDesignatorIndex(EndOfDesignatorIndex EndOfDesignatorIndex) {
        this.EndOfDesignatorIndex=EndOfDesignatorIndex;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorFirst!=null) DesignatorFirst.accept(visitor);
        if(StartOfDesignatorIndex!=null) StartOfDesignatorIndex.accept(visitor);
        if(Expr1!=null) Expr1.accept(visitor);
        if(EndOfDesignatorIndex!=null) EndOfDesignatorIndex.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorFirst!=null) DesignatorFirst.traverseTopDown(visitor);
        if(StartOfDesignatorIndex!=null) StartOfDesignatorIndex.traverseTopDown(visitor);
        if(Expr1!=null) Expr1.traverseTopDown(visitor);
        if(EndOfDesignatorIndex!=null) EndOfDesignatorIndex.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorFirst!=null) DesignatorFirst.traverseBottomUp(visitor);
        if(StartOfDesignatorIndex!=null) StartOfDesignatorIndex.traverseBottomUp(visitor);
        if(Expr1!=null) Expr1.traverseBottomUp(visitor);
        if(EndOfDesignatorIndex!=null) EndOfDesignatorIndex.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorFirstArr(\n");

        if(DesignatorFirst!=null)
            buffer.append(DesignatorFirst.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(StartOfDesignatorIndex!=null)
            buffer.append(StartOfDesignatorIndex.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr1!=null)
            buffer.append(Expr1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(EndOfDesignatorIndex!=null)
            buffer.append(EndOfDesignatorIndex.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorFirstArr]");
        return buffer.toString();
    }
}
