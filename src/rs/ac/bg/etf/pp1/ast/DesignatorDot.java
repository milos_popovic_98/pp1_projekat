// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class DesignatorDot extends Designator {

    private DesignatorFirst DesignatorFirst;
    private DesignatorChain DesignatorChain;

    public DesignatorDot (DesignatorFirst DesignatorFirst, DesignatorChain DesignatorChain) {
        this.DesignatorFirst=DesignatorFirst;
        if(DesignatorFirst!=null) DesignatorFirst.setParent(this);
        this.DesignatorChain=DesignatorChain;
        if(DesignatorChain!=null) DesignatorChain.setParent(this);
    }

    public DesignatorFirst getDesignatorFirst() {
        return DesignatorFirst;
    }

    public void setDesignatorFirst(DesignatorFirst DesignatorFirst) {
        this.DesignatorFirst=DesignatorFirst;
    }

    public DesignatorChain getDesignatorChain() {
        return DesignatorChain;
    }

    public void setDesignatorChain(DesignatorChain DesignatorChain) {
        this.DesignatorChain=DesignatorChain;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorFirst!=null) DesignatorFirst.accept(visitor);
        if(DesignatorChain!=null) DesignatorChain.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorFirst!=null) DesignatorFirst.traverseTopDown(visitor);
        if(DesignatorChain!=null) DesignatorChain.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorFirst!=null) DesignatorFirst.traverseBottomUp(visitor);
        if(DesignatorChain!=null) DesignatorChain.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorDot(\n");

        if(DesignatorFirst!=null)
            buffer.append(DesignatorFirst.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorChain!=null)
            buffer.append(DesignatorChain.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorDot]");
        return buffer.toString();
    }
}
