// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class StatementDerived7 extends Statement {

    private ContinueStatement ContinueStatement;

    public StatementDerived7 (ContinueStatement ContinueStatement) {
        this.ContinueStatement=ContinueStatement;
        if(ContinueStatement!=null) ContinueStatement.setParent(this);
    }

    public ContinueStatement getContinueStatement() {
        return ContinueStatement;
    }

    public void setContinueStatement(ContinueStatement ContinueStatement) {
        this.ContinueStatement=ContinueStatement;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ContinueStatement!=null) ContinueStatement.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ContinueStatement!=null) ContinueStatement.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ContinueStatement!=null) ContinueStatement.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("StatementDerived7(\n");

        if(ContinueStatement!=null)
            buffer.append(ContinueStatement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [StatementDerived7]");
        return buffer.toString();
    }
}
