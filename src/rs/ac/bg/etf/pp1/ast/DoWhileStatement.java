// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class DoWhileStatement implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private DoValue DoValue;
    private Statement Statement;
    private WhileValue WhileValue;
    private ConditionStart ConditionStart;
    private Condition Condition;
    private ConditionEnd ConditionEnd;

    public DoWhileStatement (DoValue DoValue, Statement Statement, WhileValue WhileValue, ConditionStart ConditionStart, Condition Condition, ConditionEnd ConditionEnd) {
        this.DoValue=DoValue;
        if(DoValue!=null) DoValue.setParent(this);
        this.Statement=Statement;
        if(Statement!=null) Statement.setParent(this);
        this.WhileValue=WhileValue;
        if(WhileValue!=null) WhileValue.setParent(this);
        this.ConditionStart=ConditionStart;
        if(ConditionStart!=null) ConditionStart.setParent(this);
        this.Condition=Condition;
        if(Condition!=null) Condition.setParent(this);
        this.ConditionEnd=ConditionEnd;
        if(ConditionEnd!=null) ConditionEnd.setParent(this);
    }

    public DoValue getDoValue() {
        return DoValue;
    }

    public void setDoValue(DoValue DoValue) {
        this.DoValue=DoValue;
    }

    public Statement getStatement() {
        return Statement;
    }

    public void setStatement(Statement Statement) {
        this.Statement=Statement;
    }

    public WhileValue getWhileValue() {
        return WhileValue;
    }

    public void setWhileValue(WhileValue WhileValue) {
        this.WhileValue=WhileValue;
    }

    public ConditionStart getConditionStart() {
        return ConditionStart;
    }

    public void setConditionStart(ConditionStart ConditionStart) {
        this.ConditionStart=ConditionStart;
    }

    public Condition getCondition() {
        return Condition;
    }

    public void setCondition(Condition Condition) {
        this.Condition=Condition;
    }

    public ConditionEnd getConditionEnd() {
        return ConditionEnd;
    }

    public void setConditionEnd(ConditionEnd ConditionEnd) {
        this.ConditionEnd=ConditionEnd;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DoValue!=null) DoValue.accept(visitor);
        if(Statement!=null) Statement.accept(visitor);
        if(WhileValue!=null) WhileValue.accept(visitor);
        if(ConditionStart!=null) ConditionStart.accept(visitor);
        if(Condition!=null) Condition.accept(visitor);
        if(ConditionEnd!=null) ConditionEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DoValue!=null) DoValue.traverseTopDown(visitor);
        if(Statement!=null) Statement.traverseTopDown(visitor);
        if(WhileValue!=null) WhileValue.traverseTopDown(visitor);
        if(ConditionStart!=null) ConditionStart.traverseTopDown(visitor);
        if(Condition!=null) Condition.traverseTopDown(visitor);
        if(ConditionEnd!=null) ConditionEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DoValue!=null) DoValue.traverseBottomUp(visitor);
        if(Statement!=null) Statement.traverseBottomUp(visitor);
        if(WhileValue!=null) WhileValue.traverseBottomUp(visitor);
        if(ConditionStart!=null) ConditionStart.traverseBottomUp(visitor);
        if(Condition!=null) Condition.traverseBottomUp(visitor);
        if(ConditionEnd!=null) ConditionEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DoWhileStatement(\n");

        if(DoValue!=null)
            buffer.append(DoValue.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Statement!=null)
            buffer.append(Statement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(WhileValue!=null)
            buffer.append(WhileValue.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConditionStart!=null)
            buffer.append(ConditionStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Condition!=null)
            buffer.append(Condition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConditionEnd!=null)
            buffer.append(ConditionEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DoWhileStatement]");
        return buffer.toString();
    }
}
