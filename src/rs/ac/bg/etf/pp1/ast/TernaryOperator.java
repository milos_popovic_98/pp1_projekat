// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class TernaryOperator extends Expr1 {

    private Expr Expr;
    private QuestionValue QuestionValue;
    private Expr Expr1;
    private ColonValue ColonValue;
    private Expr Expr2;

    public TernaryOperator (Expr Expr, QuestionValue QuestionValue, Expr Expr1, ColonValue ColonValue, Expr Expr2) {
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
        this.QuestionValue=QuestionValue;
        if(QuestionValue!=null) QuestionValue.setParent(this);
        this.Expr1=Expr1;
        if(Expr1!=null) Expr1.setParent(this);
        this.ColonValue=ColonValue;
        if(ColonValue!=null) ColonValue.setParent(this);
        this.Expr2=Expr2;
        if(Expr2!=null) Expr2.setParent(this);
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public QuestionValue getQuestionValue() {
        return QuestionValue;
    }

    public void setQuestionValue(QuestionValue QuestionValue) {
        this.QuestionValue=QuestionValue;
    }

    public Expr getExpr1() {
        return Expr1;
    }

    public void setExpr1(Expr Expr1) {
        this.Expr1=Expr1;
    }

    public ColonValue getColonValue() {
        return ColonValue;
    }

    public void setColonValue(ColonValue ColonValue) {
        this.ColonValue=ColonValue;
    }

    public Expr getExpr2() {
        return Expr2;
    }

    public void setExpr2(Expr Expr2) {
        this.Expr2=Expr2;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Expr!=null) Expr.accept(visitor);
        if(QuestionValue!=null) QuestionValue.accept(visitor);
        if(Expr1!=null) Expr1.accept(visitor);
        if(ColonValue!=null) ColonValue.accept(visitor);
        if(Expr2!=null) Expr2.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
        if(QuestionValue!=null) QuestionValue.traverseTopDown(visitor);
        if(Expr1!=null) Expr1.traverseTopDown(visitor);
        if(ColonValue!=null) ColonValue.traverseTopDown(visitor);
        if(Expr2!=null) Expr2.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        if(QuestionValue!=null) QuestionValue.traverseBottomUp(visitor);
        if(Expr1!=null) Expr1.traverseBottomUp(visitor);
        if(ColonValue!=null) ColonValue.traverseBottomUp(visitor);
        if(Expr2!=null) Expr2.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("TernaryOperator(\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(QuestionValue!=null)
            buffer.append(QuestionValue.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr1!=null)
            buffer.append(Expr1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ColonValue!=null)
            buffer.append(ColonValue.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr2!=null)
            buffer.append(Expr2.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [TernaryOperator]");
        return buffer.toString();
    }
}
