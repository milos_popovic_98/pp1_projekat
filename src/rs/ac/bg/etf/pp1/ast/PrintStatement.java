// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class PrintStatement implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private PrintStart PrintStart;
    private Expr1 Expr1;
    private OptNumConst OptNumConst;
    private PrintEnd PrintEnd;

    public PrintStatement (PrintStart PrintStart, Expr1 Expr1, OptNumConst OptNumConst, PrintEnd PrintEnd) {
        this.PrintStart=PrintStart;
        if(PrintStart!=null) PrintStart.setParent(this);
        this.Expr1=Expr1;
        if(Expr1!=null) Expr1.setParent(this);
        this.OptNumConst=OptNumConst;
        if(OptNumConst!=null) OptNumConst.setParent(this);
        this.PrintEnd=PrintEnd;
        if(PrintEnd!=null) PrintEnd.setParent(this);
    }

    public PrintStart getPrintStart() {
        return PrintStart;
    }

    public void setPrintStart(PrintStart PrintStart) {
        this.PrintStart=PrintStart;
    }

    public Expr1 getExpr1() {
        return Expr1;
    }

    public void setExpr1(Expr1 Expr1) {
        this.Expr1=Expr1;
    }

    public OptNumConst getOptNumConst() {
        return OptNumConst;
    }

    public void setOptNumConst(OptNumConst OptNumConst) {
        this.OptNumConst=OptNumConst;
    }

    public PrintEnd getPrintEnd() {
        return PrintEnd;
    }

    public void setPrintEnd(PrintEnd PrintEnd) {
        this.PrintEnd=PrintEnd;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(PrintStart!=null) PrintStart.accept(visitor);
        if(Expr1!=null) Expr1.accept(visitor);
        if(OptNumConst!=null) OptNumConst.accept(visitor);
        if(PrintEnd!=null) PrintEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(PrintStart!=null) PrintStart.traverseTopDown(visitor);
        if(Expr1!=null) Expr1.traverseTopDown(visitor);
        if(OptNumConst!=null) OptNumConst.traverseTopDown(visitor);
        if(PrintEnd!=null) PrintEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(PrintStart!=null) PrintStart.traverseBottomUp(visitor);
        if(Expr1!=null) Expr1.traverseBottomUp(visitor);
        if(OptNumConst!=null) OptNumConst.traverseBottomUp(visitor);
        if(PrintEnd!=null) PrintEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PrintStatement(\n");

        if(PrintStart!=null)
            buffer.append(PrintStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr1!=null)
            buffer.append(Expr1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptNumConst!=null)
            buffer.append(OptNumConst.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(PrintEnd!=null)
            buffer.append(PrintEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PrintStatement]");
        return buffer.toString();
    }
}
