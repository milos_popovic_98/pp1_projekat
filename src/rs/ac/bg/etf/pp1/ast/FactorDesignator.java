// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class FactorDesignator extends Factor {

    private Designator Designator;
    private OptActParsInBrackets OptActParsInBrackets;

    public FactorDesignator (Designator Designator, OptActParsInBrackets OptActParsInBrackets) {
        this.Designator=Designator;
        if(Designator!=null) Designator.setParent(this);
        this.OptActParsInBrackets=OptActParsInBrackets;
        if(OptActParsInBrackets!=null) OptActParsInBrackets.setParent(this);
    }

    public Designator getDesignator() {
        return Designator;
    }

    public void setDesignator(Designator Designator) {
        this.Designator=Designator;
    }

    public OptActParsInBrackets getOptActParsInBrackets() {
        return OptActParsInBrackets;
    }

    public void setOptActParsInBrackets(OptActParsInBrackets OptActParsInBrackets) {
        this.OptActParsInBrackets=OptActParsInBrackets;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Designator!=null) Designator.accept(visitor);
        if(OptActParsInBrackets!=null) OptActParsInBrackets.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Designator!=null) Designator.traverseTopDown(visitor);
        if(OptActParsInBrackets!=null) OptActParsInBrackets.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Designator!=null) Designator.traverseBottomUp(visitor);
        if(OptActParsInBrackets!=null) OptActParsInBrackets.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("FactorDesignator(\n");

        if(Designator!=null)
            buffer.append(Designator.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptActParsInBrackets!=null)
            buffer.append(OptActParsInBrackets.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [FactorDesignator]");
        return buffer.toString();
    }
}
