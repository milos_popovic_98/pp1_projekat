// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class ClassDecl1 extends ClassDecl {

    private ClassName ClassName;
    private OptExtends OptExtends;
    private OptFieldDecls OptFieldDecls;
    private OptMethodDecls OptMethodDecls;

    public ClassDecl1 (ClassName ClassName, OptExtends OptExtends, OptFieldDecls OptFieldDecls, OptMethodDecls OptMethodDecls) {
        this.ClassName=ClassName;
        if(ClassName!=null) ClassName.setParent(this);
        this.OptExtends=OptExtends;
        if(OptExtends!=null) OptExtends.setParent(this);
        this.OptFieldDecls=OptFieldDecls;
        if(OptFieldDecls!=null) OptFieldDecls.setParent(this);
        this.OptMethodDecls=OptMethodDecls;
        if(OptMethodDecls!=null) OptMethodDecls.setParent(this);
    }

    public ClassName getClassName() {
        return ClassName;
    }

    public void setClassName(ClassName ClassName) {
        this.ClassName=ClassName;
    }

    public OptExtends getOptExtends() {
        return OptExtends;
    }

    public void setOptExtends(OptExtends OptExtends) {
        this.OptExtends=OptExtends;
    }

    public OptFieldDecls getOptFieldDecls() {
        return OptFieldDecls;
    }

    public void setOptFieldDecls(OptFieldDecls OptFieldDecls) {
        this.OptFieldDecls=OptFieldDecls;
    }

    public OptMethodDecls getOptMethodDecls() {
        return OptMethodDecls;
    }

    public void setOptMethodDecls(OptMethodDecls OptMethodDecls) {
        this.OptMethodDecls=OptMethodDecls;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassName!=null) ClassName.accept(visitor);
        if(OptExtends!=null) OptExtends.accept(visitor);
        if(OptFieldDecls!=null) OptFieldDecls.accept(visitor);
        if(OptMethodDecls!=null) OptMethodDecls.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassName!=null) ClassName.traverseTopDown(visitor);
        if(OptExtends!=null) OptExtends.traverseTopDown(visitor);
        if(OptFieldDecls!=null) OptFieldDecls.traverseTopDown(visitor);
        if(OptMethodDecls!=null) OptMethodDecls.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassName!=null) ClassName.traverseBottomUp(visitor);
        if(OptExtends!=null) OptExtends.traverseBottomUp(visitor);
        if(OptFieldDecls!=null) OptFieldDecls.traverseBottomUp(visitor);
        if(OptMethodDecls!=null) OptMethodDecls.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDecl1(\n");

        if(ClassName!=null)
            buffer.append(ClassName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptExtends!=null)
            buffer.append(OptExtends.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptFieldDecls!=null)
            buffer.append(OptFieldDecls.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptMethodDecls!=null)
            buffer.append(OptMethodDecls.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDecl1]");
        return buffer.toString();
    }
}
