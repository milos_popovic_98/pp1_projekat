// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class DefaultStatement implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private DefaultValue DefaultValue;
    private StatementList StatementList;

    public DefaultStatement (DefaultValue DefaultValue, StatementList StatementList) {
        this.DefaultValue=DefaultValue;
        if(DefaultValue!=null) DefaultValue.setParent(this);
        this.StatementList=StatementList;
        if(StatementList!=null) StatementList.setParent(this);
    }

    public DefaultValue getDefaultValue() {
        return DefaultValue;
    }

    public void setDefaultValue(DefaultValue DefaultValue) {
        this.DefaultValue=DefaultValue;
    }

    public StatementList getStatementList() {
        return StatementList;
    }

    public void setStatementList(StatementList StatementList) {
        this.StatementList=StatementList;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DefaultValue!=null) DefaultValue.accept(visitor);
        if(StatementList!=null) StatementList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DefaultValue!=null) DefaultValue.traverseTopDown(visitor);
        if(StatementList!=null) StatementList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DefaultValue!=null) DefaultValue.traverseBottomUp(visitor);
        if(StatementList!=null) StatementList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DefaultStatement(\n");

        if(DefaultValue!=null)
            buffer.append(DefaultValue.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(StatementList!=null)
            buffer.append(StatementList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DefaultStatement]");
        return buffer.toString();
    }
}
