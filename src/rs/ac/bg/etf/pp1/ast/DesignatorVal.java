// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class DesignatorVal implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private DesignatorValName DesignatorValName;
    private OptDesignatorValArray OptDesignatorValArray;

    public DesignatorVal (DesignatorValName DesignatorValName, OptDesignatorValArray OptDesignatorValArray) {
        this.DesignatorValName=DesignatorValName;
        if(DesignatorValName!=null) DesignatorValName.setParent(this);
        this.OptDesignatorValArray=OptDesignatorValArray;
        if(OptDesignatorValArray!=null) OptDesignatorValArray.setParent(this);
    }

    public DesignatorValName getDesignatorValName() {
        return DesignatorValName;
    }

    public void setDesignatorValName(DesignatorValName DesignatorValName) {
        this.DesignatorValName=DesignatorValName;
    }

    public OptDesignatorValArray getOptDesignatorValArray() {
        return OptDesignatorValArray;
    }

    public void setOptDesignatorValArray(OptDesignatorValArray OptDesignatorValArray) {
        this.OptDesignatorValArray=OptDesignatorValArray;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorValName!=null) DesignatorValName.accept(visitor);
        if(OptDesignatorValArray!=null) OptDesignatorValArray.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorValName!=null) DesignatorValName.traverseTopDown(visitor);
        if(OptDesignatorValArray!=null) OptDesignatorValArray.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorValName!=null) DesignatorValName.traverseBottomUp(visitor);
        if(OptDesignatorValArray!=null) OptDesignatorValArray.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorVal(\n");

        if(DesignatorValName!=null)
            buffer.append(DesignatorValName.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptDesignatorValArray!=null)
            buffer.append(OptDesignatorValArray.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorVal]");
        return buffer.toString();
    }
}
