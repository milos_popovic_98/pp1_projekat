// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class ReturnStatementWithExpr extends ReturnStatement {

    private ReturnStart ReturnStart;
    private Expr1 Expr1;

    public ReturnStatementWithExpr (ReturnStart ReturnStart, Expr1 Expr1) {
        this.ReturnStart=ReturnStart;
        if(ReturnStart!=null) ReturnStart.setParent(this);
        this.Expr1=Expr1;
        if(Expr1!=null) Expr1.setParent(this);
    }

    public ReturnStart getReturnStart() {
        return ReturnStart;
    }

    public void setReturnStart(ReturnStart ReturnStart) {
        this.ReturnStart=ReturnStart;
    }

    public Expr1 getExpr1() {
        return Expr1;
    }

    public void setExpr1(Expr1 Expr1) {
        this.Expr1=Expr1;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ReturnStart!=null) ReturnStart.accept(visitor);
        if(Expr1!=null) Expr1.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ReturnStart!=null) ReturnStart.traverseTopDown(visitor);
        if(Expr1!=null) Expr1.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ReturnStart!=null) ReturnStart.traverseBottomUp(visitor);
        if(Expr1!=null) Expr1.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ReturnStatementWithExpr(\n");

        if(ReturnStart!=null)
            buffer.append(ReturnStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr1!=null)
            buffer.append(Expr1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ReturnStatementWithExpr]");
        return buffer.toString();
    }
}
