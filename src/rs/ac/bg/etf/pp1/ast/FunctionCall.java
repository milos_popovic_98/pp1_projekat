// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class FunctionCall extends Operation {

    private ActParsStart ActParsStart;
    private OptActPars OptActPars;
    private ActParsEnd ActParsEnd;

    public FunctionCall (ActParsStart ActParsStart, OptActPars OptActPars, ActParsEnd ActParsEnd) {
        this.ActParsStart=ActParsStart;
        if(ActParsStart!=null) ActParsStart.setParent(this);
        this.OptActPars=OptActPars;
        if(OptActPars!=null) OptActPars.setParent(this);
        this.ActParsEnd=ActParsEnd;
        if(ActParsEnd!=null) ActParsEnd.setParent(this);
    }

    public ActParsStart getActParsStart() {
        return ActParsStart;
    }

    public void setActParsStart(ActParsStart ActParsStart) {
        this.ActParsStart=ActParsStart;
    }

    public OptActPars getOptActPars() {
        return OptActPars;
    }

    public void setOptActPars(OptActPars OptActPars) {
        this.OptActPars=OptActPars;
    }

    public ActParsEnd getActParsEnd() {
        return ActParsEnd;
    }

    public void setActParsEnd(ActParsEnd ActParsEnd) {
        this.ActParsEnd=ActParsEnd;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ActParsStart!=null) ActParsStart.accept(visitor);
        if(OptActPars!=null) OptActPars.accept(visitor);
        if(ActParsEnd!=null) ActParsEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ActParsStart!=null) ActParsStart.traverseTopDown(visitor);
        if(OptActPars!=null) OptActPars.traverseTopDown(visitor);
        if(ActParsEnd!=null) ActParsEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ActParsStart!=null) ActParsStart.traverseBottomUp(visitor);
        if(OptActPars!=null) OptActPars.traverseBottomUp(visitor);
        if(ActParsEnd!=null) ActParsEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("FunctionCall(\n");

        if(ActParsStart!=null)
            buffer.append(ActParsStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(OptActPars!=null)
            buffer.append(OptActPars.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ActParsEnd!=null)
            buffer.append(ActParsEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [FunctionCall]");
        return buffer.toString();
    }
}
