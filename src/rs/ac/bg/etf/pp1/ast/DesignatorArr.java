// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class DesignatorArr extends Designator {

    private DesignatorFirstArr DesignatorFirstArr;
    private DesignatorChain DesignatorChain;

    public DesignatorArr (DesignatorFirstArr DesignatorFirstArr, DesignatorChain DesignatorChain) {
        this.DesignatorFirstArr=DesignatorFirstArr;
        if(DesignatorFirstArr!=null) DesignatorFirstArr.setParent(this);
        this.DesignatorChain=DesignatorChain;
        if(DesignatorChain!=null) DesignatorChain.setParent(this);
    }

    public DesignatorFirstArr getDesignatorFirstArr() {
        return DesignatorFirstArr;
    }

    public void setDesignatorFirstArr(DesignatorFirstArr DesignatorFirstArr) {
        this.DesignatorFirstArr=DesignatorFirstArr;
    }

    public DesignatorChain getDesignatorChain() {
        return DesignatorChain;
    }

    public void setDesignatorChain(DesignatorChain DesignatorChain) {
        this.DesignatorChain=DesignatorChain;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorFirstArr!=null) DesignatorFirstArr.accept(visitor);
        if(DesignatorChain!=null) DesignatorChain.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorFirstArr!=null) DesignatorFirstArr.traverseTopDown(visitor);
        if(DesignatorChain!=null) DesignatorChain.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorFirstArr!=null) DesignatorFirstArr.traverseBottomUp(visitor);
        if(DesignatorChain!=null) DesignatorChain.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorArr(\n");

        if(DesignatorFirstArr!=null)
            buffer.append(DesignatorFirstArr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorChain!=null)
            buffer.append(DesignatorChain.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorArr]");
        return buffer.toString();
    }
}
