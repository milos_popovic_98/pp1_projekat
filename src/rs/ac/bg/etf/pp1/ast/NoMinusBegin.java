// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:15


package rs.ac.bg.etf.pp1.ast;

public class NoMinusBegin extends OptMinus {

    public NoMinusBegin () {
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("NoMinusBegin(\n");

        buffer.append(tab);
        buffer.append(") [NoMinusBegin]");
        return buffer.toString();
    }
}
