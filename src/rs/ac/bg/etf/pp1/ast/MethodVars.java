// generated with ast extension for cup
// version 0.8
// 28/5/2021 9:41:14


package rs.ac.bg.etf.pp1.ast;

public class MethodVars extends OptMethodVarDecls {

    private OptMethodVarDecls OptMethodVarDecls;
    private VarDecls VarDecls;

    public MethodVars (OptMethodVarDecls OptMethodVarDecls, VarDecls VarDecls) {
        this.OptMethodVarDecls=OptMethodVarDecls;
        if(OptMethodVarDecls!=null) OptMethodVarDecls.setParent(this);
        this.VarDecls=VarDecls;
        if(VarDecls!=null) VarDecls.setParent(this);
    }

    public OptMethodVarDecls getOptMethodVarDecls() {
        return OptMethodVarDecls;
    }

    public void setOptMethodVarDecls(OptMethodVarDecls OptMethodVarDecls) {
        this.OptMethodVarDecls=OptMethodVarDecls;
    }

    public VarDecls getVarDecls() {
        return VarDecls;
    }

    public void setVarDecls(VarDecls VarDecls) {
        this.VarDecls=VarDecls;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(OptMethodVarDecls!=null) OptMethodVarDecls.accept(visitor);
        if(VarDecls!=null) VarDecls.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(OptMethodVarDecls!=null) OptMethodVarDecls.traverseTopDown(visitor);
        if(VarDecls!=null) VarDecls.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(OptMethodVarDecls!=null) OptMethodVarDecls.traverseBottomUp(visitor);
        if(VarDecls!=null) VarDecls.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MethodVars(\n");

        if(OptMethodVarDecls!=null)
            buffer.append(OptMethodVarDecls.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDecls!=null)
            buffer.append(VarDecls.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MethodVars]");
        return buffer.toString();
    }
}
