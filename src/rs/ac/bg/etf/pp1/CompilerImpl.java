package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;
import rs.ac.bg.etf.pp1.ast.Program;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CompilerImpl implements Compiler{
    private static CompilerImpl compiler;
    private List<CompilerError> compilerErrors = new ArrayList<>();

    public static CompilerImpl getCompiler() {
        if (compiler == null) {
            compiler = new CompilerImpl();
        }
        return compiler;
    }

    @Override
    public List<CompilerError> compile(String sourceFilePath, String outputFilePath) {
        try {
            File sourceCode = new File(sourceFilePath);

            Reader br = new BufferedReader(new FileReader(sourceCode));
            Yylex lexer = new Yylex(br);

            MJParser p = new MJParser(lexer);
            Symbol s = p.parse();  //pocetak parsiranja
            Program prog = (Program) (s.value);
            Tab.init();
            Tab.currentScope.addToLocals(new Obj(Obj.Type, "bool", new Struct(Struct.Bool)));

            SemanticAnalyzer v = new SemanticAnalyzer();
            prog.traverseBottomUp(v);

            if (!p.errorDetected && !v.errorDetected) {
                File objFile = new File(outputFilePath);
                if (objFile.exists()) objFile.delete();
                CodeGenerator codeGenerator = new CodeGenerator();
                prog.traverseBottomUp(codeGenerator);
                Code.mainPc = codeGenerator.getMainPc();
                Code.write(new FileOutputStream(objFile));
            }
            return compilerErrors;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addCompilerError(CompilerError error) {
        compilerErrors.add(error);
    }
}
