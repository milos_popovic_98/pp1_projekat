package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.DesignatorFirst;
import rs.ac.bg.etf.pp1.ast.DesignatorVal;
import rs.ac.bg.etf.pp1.ast.DesignatorValName;
import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.etf.pp1.symboltable.concepts.Obj;

public class DesignatorNode {
    private SyntaxNode syntaxNode;
    private String name;

    public DesignatorNode(SyntaxNode syntaxNode, String name) {
        this.syntaxNode = syntaxNode;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setObj(Obj obj) {
        if (syntaxNode instanceof DesignatorFirst) {
            ((DesignatorFirst) syntaxNode).obj = obj;
        } else if (syntaxNode instanceof DesignatorVal) {
            ((DesignatorVal)syntaxNode).obj = obj;
        } else {
            System.out.println("Moja greska");
        }
    }
}
