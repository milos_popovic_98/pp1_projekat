package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;
import rs.ac.bg.etf.pp1.ast.YieldStatement;
import rs.etf.pp1.symboltable.concepts.Struct;


public class YieldVisitor extends VisitorAdaptor {

    private SemanticAnalyzer semanticAnalyzer;
    private Struct leftType;

    public YieldVisitor(SemanticAnalyzer semanticAnalyzer, Struct leftType) {
        this.semanticAnalyzer = semanticAnalyzer;
        this.leftType = leftType;
    }

    public void visit(YieldStatement yieldStatement) {
        if (!semanticAnalyzer.checkTypeCompatibility(leftType, yieldStatement.getExpr1().struct)) {
            semanticAnalyzer.report_error("Greska na liniji " + yieldStatement.getLine() + " : "
                    + "nekompatibilni tipovi u dodeli vrednosti! ", null);
        }
    }
}
