package rs.ac.bg.etf.pp1;

import java.util.ArrayList;
import java.util.List;

public class TernaryOperatorContext extends BranchContext{
    private List<Integer> branchesAfter = new ArrayList<>();
    private List<Integer> branchesOn = new ArrayList<>();
    private List<Integer> thenBranches = new ArrayList<>();

    @Override
    public void addBranchOn(int adr) {
        branchesOn.add(adr);
    }

    @Override
    public void addBranchAfter(int adr) {
        branchesAfter.add(adr);
    }

    @Override
    public void addBranchThen(int adr) {
        thenBranches.add(adr);
    }

    @Override
    public List<Integer> getBranchesOn() {
        return branchesOn;
    }

    @Override
    public List<Integer> getBranchesAfter() {
        return branchesAfter;
    }

    @Override
    public List<Integer> getBranchesThen() {
        return thenBranches;
    }

}
