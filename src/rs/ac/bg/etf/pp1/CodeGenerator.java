package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

import java.util.List;
import java.util.Stack;

public class CodeGenerator extends VisitorAdaptor {
    enum State {
        LEFT_SIDE, RIGHT_SIDE, INDEX, PRINT, CONDITION, SWITCH, RETURN, ACT_PARS, TERNARY
    }

    private int mainPc;
    private String progName;
    private Obj previousObjInChain;

    private State state;
    private boolean isArrayIndexed;
    private final Stack<Context> contextStack = new Stack<>();
    private final Stack<BranchContext> branchContextStack = new Stack<>();
    private BranchContext branchContext;

    private int lastBranchCond = 0;

    public int getMainPc() {
        return mainPc;
    }

    public void visit(ProgName progName) {
        this.progName = progName.getProgName();
        //chr
        Obj chr = Tab.find("chr");
        chr.setAdr(Code.pc);
        Code.put(Code.enter);
        Code.put(chr.getFpPos());
        Code.put(chr.getLocalSymbols().size());
        Code.put(Code.load_n);
        Code.put(Code.exit);
        Code.put(Code.return_);
        //ord
        Obj ord = Tab.find("ord");
        ord.setAdr(Code.pc);
        Code.put(Code.enter);
        Code.put(chr.getFpPos());
        Code.put(chr.getLocalSymbols().size());
        Code.put(Code.load_n);
        Code.put(Code.exit);
        Code.put(Code.return_);
        //len
        Obj len = Tab.find("len");
        len.setAdr(Code.pc);
        Code.put(Code.enter);
        Code.put(chr.getFpPos());
        Code.put(chr.getLocalSymbols().size());
        Code.put(Code.load_n);
        Code.put(Code.arraylength);
        Code.put(Code.exit);
        Code.put(Code.return_);
    }

    public void visit(PrintStatement printStatement) {
        if (printStatement.getExpr1().struct == Tab.charType) {
            Code.loadConst(1);
            Code.put(Code.bprint);
        } else {
            Code.loadConst(5);
            Code.put(Code.print);
        }
    }

    public void visit(NumConst numConst) {
        Code.loadConst(numConst.getNumber());
    }

    public void visit(ConstValueNumber constValueNumber) {
        SyntaxNode parent = constValueNumber.getParent();
        if (parent.getClass() == FactorConst.class) {
            Code.loadConst(constValueNumber.getNumber());
        }
    }

    public void visit(ConstValueBool constValueBool) {
        SyntaxNode parent = constValueBool.getParent();
        if (parent.getClass() == FactorConst.class) {
            Obj con = Tab.insert(Obj.Con, "$", constValueBool.struct);
            con.setAdr(constValueBool.getBool().equals("true") ? 1 : 0);
            con.setLevel(0);

            Code.load(con);
        }
    }

    public void visit(ConstValueChar constValueChar) {
        SyntaxNode parent = constValueChar.getParent();
        if (parent.getClass() == FactorConst.class) {
            Code.loadConst(constValueChar.getCharacter());
        }
    }

    public void visit(MethodTypeName methodTypeName) {
        if ("main".equalsIgnoreCase(methodTypeName.getMethodName())) {
            mainPc = Code.pc;
            TVF.getTVF().fillTable(progName);
        }
        methodTypeName.obj.setAdr(Code.pc);
        int numberOfFormalArgs = methodTypeName.obj.getFpPos();
        int numberOfAllVars = methodTypeName.obj.getLocalSymbols().size();

        Code.put(Code.enter);
        Code.put(numberOfFormalArgs);
        Code.put(numberOfAllVars);
    }

    public void visit(MethodDecl methodDecl) {
        Obj method = methodDecl.getMethodTypeName().obj;
        if (method.getType().equals(Tab.noType)) {
            Code.put(Code.exit);
            Code.put(Code.return_);
        } else {
            Code.put(Code.trap);
            Code.put(-1);
        }
    }

    public void visit(AssignOperation assignOperation) {
        restoreContext();
        if (isArrayIndexed) {
            if (previousObjInChain.getType().getElemType().getKind() == Struct.Char) {
                Code.put(Code.bastore);
            } else {
                Code.put(Code.astore);
            }
            isArrayIndexed = false;
        } else {
            Code.store(previousObjInChain);
        }
        NewVisitor visitor = new NewVisitor();
        assignOperation.traverseTopDown(visitor);
        if (visitor.getStatus()) {
            addTvfToNewObject(assignOperation, visitor.getType());
        }
        previousObjInChain = null;
    }

    protected void addTvfToNewObject(AssignOperation assignOperation, Struct type) {
        DesignatorStatement1 ds = (DesignatorStatement1) assignOperation.getParent();
        Designator designator = ds.getDesignator();
        saveContext(State.RIGHT_SIDE);
        designator.traverseBottomUp(this);
        int tvfAdr = TVF.getTVF().getTvfStartAdr(type);
        Code.loadConst(tvfAdr);
        Code.put(Code.putfield);
        Code.put2(0);
        restoreContext();
    }

    public void visit(FactorDesignator factorDesignator) {
        if (previousObjInChain.getKind() == Obj.Meth) {
            boolean globalMethod = previousObjInChain.getLevel() == 0;
            if (globalMethod) {
                callGlobalMethod();
            } else {
                callClassMethod(factorDesignator.getDesignator());
            }
        }
    }

    public void visit(DesignatorStatement1 designatorStatement) {
        Operation operation = designatorStatement.getOperation();
        Designator designator = designatorStatement.getDesignator();
        if (operation instanceof Increment || operation instanceof Decrement) {
            int constVal = operation instanceof Increment ? 1 : -1;
            designator.traverseBottomUp(this);
            if (isArrayIndexed) {
                if (previousObjInChain.getType().getKind() == Struct.Char) {
                    Code.put(Code.baload);
                    Code.loadConst(constVal);
                    Code.put(Code.add);
                    Code.put(Code.bastore);
                } else {
                    Code.put(Code.aload);
                    Code.loadConst(constVal);
                    Code.put(Code.add);
                    Code.put(Code.astore);
                }
                isArrayIndexed = false;
            } else {
                Code.load(previousObjInChain);
                Code.loadConst(constVal);
                Code.put(Code.add);
                Code.store(previousObjInChain);
            }
            previousObjInChain = null;
        }
    }

    public void visit(SumImpl sum) {
        Addop addop = sum.getAddop();
        if (addop instanceof Plus) {
            Code.put(Code.add);
        } else {
            Code.put(Code.sub);
        }
    }

    public void visit(MulTerm mulTerm) {
        Mulop mulop = mulTerm.getMulop();
        if (mulop instanceof Mul) {
            Code.put(Code.mul);
        } else if (mulop instanceof Div) {
            Code.put(Code.div);
        } else {
            Code.put(Code.rem);
        }
    }

    public void visit(TermImpl term) {
        if (MinusBegin.class == term.getOptMinus().getClass()) {
            Code.put(Code.neg);
        }
    }

    public void visit(FactorNew factorNew) {
        Struct type = factorNew.struct;
        if (factorNew.getOptExprInSquares() instanceof ExprInSquares) {
            Code.put(Code.newarray);
            if (type.equals(Tab.charType)) {
                Code.put(0);
            } else {
                Code.put(1);
            }
        } else {
            int numOfFields = type.getNumberOfFields();
            int size = numOfFields * 4;
            Code.put(Code.new_);
            Code.put2(size);
        }
    }

    public void visit(DesignatorValName designatorValName) {
        Obj obj = ((DesignatorVal) designatorValName.getParent()).obj;
        if (state != State.LEFT_SIDE) {
            if (obj.getKind() != Obj.Meth)
                Code.load(obj);
        } else {
            if (isArrayIndexed) {
                if (previousObjInChain.getType().getKind() == Struct.Char) {
                    Code.put(Code.baload);
                } else {
                    Code.put(Code.aload);
                }
                isArrayIndexed = false;
            } else {
                Code.load(previousObjInChain);
            }
        }
        previousObjInChain = obj;
    }

    public void visit(StartOfDesignatorIndex start) {
        if (state == State.LEFT_SIDE)
            Code.load(previousObjInChain);
        saveContext(State.INDEX);
    }

    public void visit(EndOfDesignatorIndex end) {
        restoreContext();
        if (state != State.LEFT_SIDE) {
            if (previousObjInChain.getType().getElemType().getKind() == Struct.Char) {
                Code.put(Code.baload);
            } else {
                Code.put(Code.aload);
            }
            isArrayIndexed = false;
        }
    }

    public void visit(Assignop assignop) {
        saveContext(State.RIGHT_SIDE);
    }

    public void visit(DesignatorFirst designator) {
        int kind = designator.obj.getKind();

        if (kind == Obj.Fld) {
            Code.put(Code.load_n);
        } else if (kind == Obj.Meth && designator.obj.getLevel() > 0) {
            Code.put(Code.load_n);
        }

        if (state != State.LEFT_SIDE) {
            if (kind != Obj.Meth)
                Code.load(designator.obj);
        }
        previousObjInChain = designator.obj;
    }

    public void visit(PrintStart printStart) {
        saveContext(State.PRINT);
    }

    public void visit(PrintEnd printEnd) {
        restoreContext();
    }

    public void visit(DesignatorFirstArr designator) {
        isArrayIndexed = true;
    }

    public void visit(DesignatorValArray designator) {
        isArrayIndexed = true;
    }

    public void visit(RelopEq eq) {
        lastBranchCond = Code.eq;
    }

    public void visit(RelopNeq neq) {
        lastBranchCond = Code.ne;
    }

    public void visit(RelopGe ge) {
        lastBranchCond = Code.ge;
    }

    public void visit(RelopGt gt) {
        lastBranchCond = Code.gt;
    }

    public void visit(RelopLe le) {
        lastBranchCond = Code.le;
    }

    public void visit(RelopLt lt) {
        lastBranchCond = Code.lt;
    }

    public void visit(AndCond and) {
        Code.putFalseJump(lastBranchCond, Code.pc);
        branchContext.addBranchAfter(Code.pc - 2);
    }

    public void visit(OrCond or) {
        Code.putFalseJump(Code.inverse[lastBranchCond], Code.pc);
        branchContext.addBranchOn(Code.pc - 2);

        fixupBranches(branchContext.getBranchesAfter());
    }

    public void visit(ConditionStart cond) {
        saveContext(State.CONDITION);
    }

    public void visit(ConditionEnd cond) {
        Code.putFalseJump(lastBranchCond, Code.pc);
        branchContext.addBranchAfter(Code.pc - 2);

        fixupBranches(branchContext.getBranchesOn());
        restoreContext();
    }

    public void visit(CondFactExpr expr) {
        Code.loadConst(1);
        lastBranchCond = Code.eq;
    }

    public void visit(IfStart ifStart) {
        branchContextStack.push(branchContext);
        branchContext = new IfContext();
    }

    public void visit(ElseBranch elseBranch) {
        Code.putJump(Code.pc);
        branchContext.addBranchThen(Code.pc - 2);
        fixupBranches(branchContext.getBranchesAfter());
    }

    public void visit(IfThenStatement stmt) {
        fixupBranches(branchContext.getBranchesAfter());
        branchContext = branchContextStack.pop();
    }

    public void visit(IfElseStatement ifElseStatement) {
        fixupBranches(branchContext.getBranchesThen());
        branchContext = branchContextStack.pop();
    }

    protected void fixupBranches(List<Integer> branches) {
        for (Integer patchAdr : branches) {
            Code.fixup(patchAdr);
        }
        branches.clear();
    }

    public void visit(SwitchStart start) {
        branchContextStack.push(branchContext);
        branchContext = new SwitchContext();

        SyntaxNode parent = start.getParent();
        Expr1 expr = null;
        if (parent instanceof SwitchStatement) {
            expr = ((SwitchStatement) parent).getExpr1();
        } else if (parent instanceof SwitchYieldExpr) {
            expr = ((SwitchYieldExpr) parent).getExpr1();
        }
        ((SwitchContext) branchContext).setExpr(expr);
    }

    public void visit(YieldStatement yieldStatement) {
        Code.putJump(Code.pc);
        BranchContext context = branchContext;
        int i = branchContextStack.size() - 1;
        while (i >= 0 && !(context instanceof SwitchContext)) {
            context = branchContextStack.get(i);
            i--;
        }
        context.addBranchThen(Code.pc - 2);
    }

    public void visit(SwitchYieldExpr switchExpr) {
        fixupSwitchBranches();
    }

    public void visit(SwitchStatement switchStatement) {
        fixupSwitchBranches();
    }

    protected void fixupSwitchBranches() {
        fixupBranches(branchContext.getBranchesAfter());
        fixupBranches(branchContext.getBranchesOn());
        fixupBranches(branchContext.getBranchesThen());
        branchContext = branchContextStack.pop();
    }

    public void visit(SwitchExprStart start) {
        saveContext(State.SWITCH);
    }

    public void visit(SwitchExprEnd end) {
        restoreContext();
    }

    public void visit(NoCases noCases) {
        if (!(noCases.getParent() instanceof CaseListImpl)) {
            Code.loadConst(0);
            Code.put(Code.jcc);
            Code.put2(3);
        }
    }

    public void visit(CaseStart caseStart) {
        CaseStatement stmt = (CaseStatement) caseStart.getParent();
        int constValue = stmt.getNumConst();
        SwitchContext context = (SwitchContext) branchContext;
        if (context.isFirstCase()) {
            context.setFirstCase(false);
        } else {
            fixupBranches(context.getBranchesAfter());
            State stateTemp = state;
            state = State.SWITCH;
            context.getExpr().traverseBottomUp(this);
            state = stateTemp;
        }
        Code.loadConst(constValue);
        Code.putFalseJump(Code.eq, Code.pc);
        branchContext.addBranchAfter(Code.pc - 2);
        fixupBranches(context.getBranchesOn());
    }

    public void visit(DefaultValue defaultValue) {
        fixupBranches(branchContext.getBranchesAfter());
    }

    public void visit(CaseStatement caseStatement) {
        Code.putJump(Code.pc);
        branchContext.addBranchOn(Code.pc - 2);
    }

    public void visit(DoValue doValue) {
        branchContextStack.push(branchContext);
        DoWhileContext context = new DoWhileContext();
        context.setDoLabelAdr(Code.pc);
        branchContext = context;
    }

    public void visit(WhileValue whileValue) {
        DoWhileContext context = (DoWhileContext) branchContext;
        fixupBranches(context.getContinueBranches());
    }

    public void visit(DoWhileStatement statement) {
        int doLabelAdr = ((DoWhileContext) branchContext).getDoLabelAdr();
        fixupDoBranches(branchContext.getBranchesOn(), doLabelAdr);
        Code.putJump(doLabelAdr);
        fixupBranches(branchContext.getBranchesAfter());
        fixupBranches(branchContext.getBranchesThen());

        branchContext = branchContextStack.pop();
    }

    protected void fixupDoBranches(List<Integer> branches, int doLabelAdr) {
        for (Integer patchAdr : branches) {
            Code.put2(patchAdr, (doLabelAdr - patchAdr + 1));
        }
        branches.clear();
    }

    public void visit(ContinueStatement statement) {
        Code.putJump(Code.pc);
        BranchContext context = branchContext;
        int i = branchContextStack.size() - 1;
        while (i >= 0 && !(context instanceof DoWhileContext)) {
            context = branchContextStack.get(i);
            i--;
        }
        ((DoWhileContext) context).addContinueBranch(Code.pc - 2);
    }

    public void visit(BreakStatement statement) {
        Code.putJump(Code.pc);
        BranchContext context = branchContext;
        int i = branchContextStack.size() - 1;
        while (i >= 0 && !(context instanceof DoWhileContext)
                && !(context instanceof SwitchContext)) {
            context = branchContextStack.get(i);
            i--;
        }
        context.addBranchThen(Code.pc - 2);
    }

    public void visit(ReturnStart start) {
        saveContext(State.RETURN);
    }

    public void visit(ReturnStatementWithExpr returnStatement) {
        Code.put(Code.exit);
        Code.put(Code.return_);
        restoreContext();
    }

    public void visit(ReturnStatement returnStatement) {
        Code.put(Code.exit);
        Code.put(Code.return_);
    }

    protected void saveContext(State newState) {
        contextStack.push(new Context(state, previousObjInChain, isArrayIndexed));
        previousObjInChain = null;
        isArrayIndexed = false;
        state = newState;
    }

    protected void restoreContext() {
        Context context = contextStack.pop();
        previousObjInChain = context.getPreviousObjInChain();
        isArrayIndexed = context.getIsArrayIndexed();
        state = context.getState();
    }

    public void visit(ActParsStart start) {
        saveContext(State.ACT_PARS);
    }

    public void visit(ActParsEnd end) {
        restoreContext();
    }

    public void visit(FunctionCall functionCall) {
        boolean globalMethod = previousObjInChain.getLevel() == 0;
        if (globalMethod) {
            callGlobalMethod();
        } else {
            DesignatorStatement1 ds = (DesignatorStatement1) functionCall.getParent();
            Designator designator = ds.getDesignator();
            callClassMethod(designator);
            if (previousObjInChain.getType().getKind() != Struct.None) {
                Code.put(Code.pop);
            }
        }
    }

    protected void callGlobalMethod() {
        Code.put(Code.call);
        Code.put2(previousObjInChain.getAdr() - Code.pc + 1);
    }

    protected void callClassMethod(Designator designator) {
        saveContext(State.LEFT_SIDE);
        designator.traverseBottomUp(this);
        restoreContext();
        Code.put(Code.getfield);
        Code.put2(0);
        Code.put(Code.invokevirtual);
        pushMethodName(previousObjInChain.getName());
    }

    protected void pushMethodName(String name) {
        for (int i = 0; i < name.length(); i++) {
            Code.put4(name.charAt(i));
        }
        Code.put4(-1);
    }

    public void visit(ReadStatement readStatement) {
        int kind = previousObjInChain.getType().getKind();

        if (kind == Struct.Char) {
            Code.put(Code.bread);
        } else {
            Code.put(Code.read);
        }

        if (isArrayIndexed) {
            if (kind == Struct.Char) {
                Code.put(Code.bastore);
            } else {
                Code.put(Code.astore);
            }
            isArrayIndexed = false;
        } else {
            Code.store(previousObjInChain);
        }
        previousObjInChain = null;
    }

    public void visit(QuestionValue value) {
        saveContext(State.TERNARY);
        branchContext = new TernaryOperatorContext();
        Code.loadConst(1);
        Code.putFalseJump(Code.eq, Code.pc);
        branchContext.addBranchAfter(Code.pc - 2);
    }

    public void visit(ColonValue value) {
        Code.putJump(Code.pc);
        branchContext.addBranchThen(Code.pc - 2);
        fixupBranches(branchContext.getBranchesAfter());
    }

    public void visit(TernaryOperator operator) {
        fixupBranches(branchContext.getBranchesThen());
        restoreContext();
    }
}
