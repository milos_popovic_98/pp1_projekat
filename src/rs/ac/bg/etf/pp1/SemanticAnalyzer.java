package rs.ac.bg.etf.pp1;

import org.apache.log4j.Logger;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;
import rs.etf.pp1.symboltable.structure.HashTableDataStructure;
import rs.etf.pp1.symboltable.structure.SymbolDataStructure;

import java.util.*;

public class SemanticAnalyzer extends VisitorAdaptor {
    private final static Struct boolType = Tab.find("bool").getType();

    int printCallCount;
    int varDeclCount;
    boolean mainFound;
    Obj currentMethod;
    Obj currentClass;
    boolean returnFound;
    boolean errorDetected;
    int nVars;
    boolean isArray;
    Struct currentType;
    int currentConstValue;
    int level = 0;
    int numOfFormalArgs = 0;
    Queue<DesignatorNode> designatorChainQueue = new ArrayDeque<>();
    Stack<Queue<DesignatorNode>> designatorChainQueueStack = new Stack<>();
    Set<Integer> caseList = new HashSet<>();
    List<Struct> actParamTypes = new ArrayList<>();
    Logger log = Logger.getLogger(getClass());

    public void report_error(String message, SyntaxNode info) {
        errorDetected = true;
        StringBuilder msg = new StringBuilder("Semanticka greska ");
        int line = (info == null) ? 0 : info.getLine();
        if (line != 0)
            msg.append(" na liniji ").append(line);
        msg.append(": ").append(message);
        log.error(msg.toString());

        CompilerError error = new CompilerError(line, msg.toString(), CompilerError.CompilerErrorType.SEMANTIC_ERROR);
        CompilerImpl compiler = CompilerImpl.getCompiler();
        compiler.addCompilerError(error);
    }

    public void report_info(String message, SyntaxNode info) {
        StringBuilder msg = new StringBuilder(message);
        int line = (info == null) ? 0 : info.getLine();
        if (line != 0)
            msg.append(" na liniji ").append(line);
        log.info(msg.toString());
    }

    public void report_info_detected_symbol(Obj obj, int kind, int line) {
        if (obj != null) {
            StringBuilder msg = new StringBuilder("Detektovan simbol - ");
            msg.append("ime: " + obj.getName() + ", ");
            msg.append("vrsta: " + convertObjKind(kind) + ", ");
            msg.append("tip: " + convertTypeKind(obj.getType().getKind()));
            msg.append(" na liniji " + line);
            log.info(msg.toString());
        }
    }

    public String convertTypeKind(int kind) {
        switch (kind) {
            case Struct.None:
                return "void";
            case Struct.Int:
                return "int";
            case Struct.Char:
                return "char";
            case Struct.Array:
                return "array";
            case Struct.Class:
                return "class";
            case Struct.Bool:
                return "bool";
            default:
                return "greska";
        }
    }

    public String convertObjKind(int kind) {
        switch (kind) {
            case Obj.Con:
                return "konstanta";
            case Obj.Var:
                return "promenjiva";
            case Obj.Type:
                return "tip";
            case Obj.Meth:
                return "funkcija";
            case Obj.Fld:
                return "polje klase";
            case Obj.Elem:
                return "element niza";
            case Obj.Prog:
                return "program";
            default:
                return "greska";
        }
    }

    public void visit(ProgName progName) {
        progName.obj = Tab.insert(Obj.Prog, progName.getProgName(), Tab.noType);
        Obj chr = Tab.find("chr");
        chr.setFpPos(1);
        chr.setLevel(0);

        Obj ord = Tab.find("ord");
        ord.setFpPos(1);
        ord.setLevel(0);

        Obj len = Tab.find("len");
        len.setFpPos(1);
        len.setLevel(0);

        Tab.openScope();
    }

    public void visit(Program program) {
        nVars = Tab.currentScope.getnVars();
        Tab.chainLocalSymbols(program.getProgName().obj);
        TVF.getTVF().setNumberOfGlobalVariables(nVars);
        if (!mainFound) {
            report_error("main funkcija nije definisana", null);
            errorDetected = true;
        }
        Tab.closeScope();
    }

    public void visit(ClassName className) {
        Obj obj = Tab.find(className.getName());
        if (obj == Tab.noObj) {
            className.obj = Tab.insert(Obj.Type, className.getName(), new Struct(Struct.Class));
            currentClass = className.obj;
            Tab.openScope();
            Obj tvf = Tab.insert(Obj.Fld, "tvf", Tab.intType);
            level++;
            tvf.setLevel(level);
        } else {
            report_error("klasa sa istim imenom " + className.getName() + " je vec ranije deklarisana.", className);
        }
    }

    public void visit(Extends extend) {
        Struct typeNode = extend.getType().struct;
        if (typeNode.getKind() == Struct.None) return;
        if (typeNode.getKind() == Struct.Class) {
            currentClass.getType().setElementType(typeNode);
            Collection<Obj> members = typeNode.getMembers();
            for (Obj member : members) {
                if (member.getKind() == Obj.Meth) {
                    continue;
                } else {
                    if (!"tvf".equals(member.getName()))
                        Tab.insert(member.getKind(), member.getName(), member.getType());
                }
            }
        } else {
            report_error("tip " + extend.getType().getTypeName()
                    + " nije tipa klase i ne moze se korisiti pri izvodjenju.", extend);
        }
    }

    public void visit(ClassDecl1 classDecl) {
        if (currentClass != null) {
            nVars = Tab.currentScope().getnVars();
            Tab.chainLocalSymbols(classDecl.getClassName().obj.getType());
            checkOverrideMethodsAndAddMethodsFromBaseClass(classDecl);
            Tab.closeScope();
            currentClass = null;
            level--;
        }
    }

    protected void checkOverrideMethodsAndAddMethodsFromBaseClass(SyntaxNode node) {
        Struct derivedClass = currentClass.getType();
        Struct baseClass = currentClass.getType().getElemType();
        if (baseClass == null) return;
        Collection<Obj> baseClassMembers = baseClass.getMembers();
        SymbolDataStructure derivedClassMembers = derivedClass.getMembersTable();
        for (Obj baseMember : baseClassMembers) {
            if (baseMember.getKind() == Obj.Meth) {
                Obj derivedMember = derivedClassMembers.searchKey(baseMember.getName());
                if (derivedMember == null) addMethodFromBaseClass(baseMember);
                else if (derivedMember.getKind() != Obj.Meth) continue;
                else {
                    if (!checkTypeCompatibility(baseMember.getType(), derivedMember.getType())) {
                        report_error("redefinisana metoda " + derivedMember.getName() + " u klasi " + currentClass.getName()
                                + " nema istu povratnu vrednost kao metoda osnovne klase.", null);
                    } else {
                        checkFormalParams(baseMember, derivedMember);
                    }
                }
            }
        }
    }

    protected void checkFormalParams(Obj baseMember, Obj derivedMember) {
        int numberOfFormalArgs = baseMember.getFpPos();
        if (derivedMember.getFpPos() != numberOfFormalArgs) {
            report_error("redefinisana metoda " + derivedMember.getName() + " u klasi "
                    + currentClass.getName() + " nema jednak broj parametara kao metoda u osnovnoj klasi.", null);
            return;
        }
        Collection<Obj> baseParams = baseMember.getLocalSymbols();
        Collection<Obj> derivedParams = derivedMember.getLocalSymbols();
        Iterator<Obj> i1 = baseParams.iterator();
        Iterator<Obj> i2 = derivedParams.iterator();
        int counter = 0;
        while (i1.hasNext() && i2.hasNext()) {
            if (numberOfFormalArgs == counter) break;
            Obj o1 = i1.next();
            Obj o2 = i2.next();
            counter++;
            if (counter == 1) continue;
            if (!checkTypeCompatibility(o1.getType(), o2.getType())) {
                report_error("redefinisana metoda " + derivedMember.getName() + "u klasi "
                        + currentClass.getName() + " nema isti potpis kao metoda osnovne klase.", null);
            }
        }
    }

    protected void addMethodFromBaseClass(Obj baseMember) {
        SymbolDataStructure members = currentClass.getType().getMembersTable();
        members.insertKey(baseMember);
    }

    public void visit(PrintStatement printStatement) {
        Struct struct = printStatement.getExpr1().struct;
        if (struct != Tab.charType && struct != Tab.intType && struct != boolType) {
            report_error("operand funkcije PRINT mora biti ili char ili int ili bool tipa", printStatement);
        }
    }

    public void visit(ReadStatement readStatement) {
        Obj designator = readStatement.getDesignator().obj;
        int kind = designator.getKind();
        Struct type = designator.getType();
        if (kind != Obj.Var && kind != Obj.Fld || designator.getType().getKind() == Struct.Array) {
            report_error("operand funkcije READ mora oznacavati promenljivu, element niza ili polje objekta"
                    , readStatement);
        }
        if (type != Tab.charType && type != Tab.intType && type != Tab.find("bool").getType()) {
            report_error("operand funkcije READ mora biti char ili int ili bool tipa", readStatement);
        }
    }

    public void visit(ReturnStatementWithExpr returnStatement) {
        if (currentMethod != null) {
            Struct currMethType = currentMethod.getType();
            Struct currRetType = returnStatement.getExpr1().struct;
            if (!checkTypeCompatibility(currMethType, currRetType)) {
                report_error("tip izraza u return naredbi ne slaze se sa tipom povratne vrednosti funkcije " + currentMethod.getName(), returnStatement);
            }
            returnFound = true;
        } else {
            report_error("pronadjena naredba return izvan funkcije.", returnStatement);
        }
    }

    public void visit(ReturnStatement returnStatement) {
        if (currentMethod != null) {
            Struct currMethType = currentMethod.getType();
            if (!currMethType.equals(Tab.noType)) {
                report_error("pronadjena prazna return naredba u funkciji "
                        + currentMethod.getName() + " koja ima povratnu vrednost.", returnStatement.getParent());
            }
            returnFound = true;
        } else {
            report_error("pronadjena naredba return izvan funkcije!", returnStatement.getParent());
        }
    }

    public void visit(ConstType constType) {
        Struct type = constType.getType().struct;
        if (type.equals(Tab.intType) || type.equals(Tab.charType) || type.equals(boolType)) {
            currentType = constType.getType().struct;
        } else {
            report_error(constType.getType().getTypeName() + " nije odgovarajuci tip za konstantu.", constType);
            currentType = Tab.noType;
        }
    }

    public void visit(Type type) {
        Obj typeNode = Tab.find(type.getTypeName());
        if (typeNode == Tab.noObj || typeNode.getKind() != Obj.Type) {
            report_error(type.getTypeName() + " ne predstavlja tip.", type);
            type.struct = Tab.noType;
        } else {
            type.struct = typeNode.getType();
            currentType = type.struct;
        }
    }


    public void visit(ConstVar constVar) {
        Obj constNode = Tab.find(constVar.getConstName());
        if (constNode == Tab.noObj) {
            if (constVar.getConstValue().struct.equals(currentType)) {
                constNode = Tab.insert(Obj.Con, constVar.getConstName(), currentType);
                constNode.setAdr(currentConstValue);
                currentConstValue = 0;
            } else {
                report_error("greska pri dodeli vrednosti konstanti, tipovi nisu kompatiblni.", constVar);
            }
        } else {
            report_error("konstanta sa istim imenom " + constVar.getConstName() + " je ranije deklarisana.", constVar);
        }
    }

    public void visit(ConstValueNumber constValueNumber) {
        this.currentConstValue = constValueNumber.getNumber();
        constValueNumber.struct = Tab.intType;
    }

    public void visit(ConstValueBool constValueBool) {
        if ("true".equals(constValueBool.getBool()))
            this.currentConstValue = 1;
        else
            this.currentConstValue = 0;
        constValueBool.struct = boolType;
    }

    public void visit(ConstValueChar constValueChar) {
        this.currentConstValue = constValueChar.getCharacter();
        constValueChar.struct = Tab.charType;
    }

    public void visit(ConstDecls constDecls) {
        this.currentType = null;
    }

    public void visit(Var1 var) {
        Obj varNode = Tab.find(var.getVarName());
        if (varNode == Tab.noObj || varNode.getLevel() != level) {
            int kind;
            if (currentClass != null && currentMethod == null) {
                kind = Obj.Fld;
            } else {
                kind = Obj.Var;
            }
            Obj obj;
            if (isArray) {
                obj = Tab.insert(kind, var.getVarName(), new Struct(Struct.Array, currentType));
                isArray = false;
            } else {
                obj = Tab.insert(kind, var.getVarName(), currentType);
            }
            obj.setLevel(level);
        } else {
            report_error("promenljiva " + var.getVarName() + " je vec deklarisana", var);
        }
    }

    public void visit(Array array) {
        this.isArray = true;
    }

    public void visit(MethodTypeName methodTypeName) {
        Obj obj1 = Tab.find(methodTypeName.getMethodName());
        if (obj1 == Tab.noObj) {
            currentMethod = Tab.insert(Obj.Meth, methodTypeName.getMethodName(), methodTypeName.getMethodType().struct);
            methodTypeName.obj = currentMethod;
            level++;
            Tab.openScope();
            if (currentClass != null) {
                Obj obj = Tab.insert(Obj.Var, "this", currentClass.getType());
                obj.setLevel(level);
                obj.setFpPos(0);
                numOfFormalArgs++;
            }
            if ("main".equalsIgnoreCase(methodTypeName.getMethodName())) {
                mainFound = true;
                if (!methodTypeName.getMethodType().struct.equals(Tab.noType)) {
                    report_error("main metoda mora biti deklarisana kao void metoda.", methodTypeName);
                }
            }
            report_info("Obradjuje se funkcija " + methodTypeName.getMethodName(), methodTypeName);
        } else {
            report_error("funkcija sa istim imenom " + methodTypeName.getMethodName() + " je vec definisana u opsegu", methodTypeName);
        }
    }

    public void visit(MethodDecl methodDecl) {
        if (currentMethod != null) {
            if (!returnFound && currentMethod.getType() != Tab.noType) {
                report_error("funkcija " + currentMethod.getName() + " nema return iskaz!", methodDecl);
            }
            Tab.chainLocalSymbols(currentMethod);
            currentMethod.setLevel(currentClass != null ? 1 : 0);
            currentMethod.setFpPos(numOfFormalArgs);
            Tab.closeScope();

            level--;
            currentMethod = null;
            numOfFormalArgs = 0;
            returnFound = false;
        }
    }

    public void visit(MethodTypeVoid methodTypeVoid) {
        methodTypeVoid.struct = Tab.noType;
    }

    public void visit(MethodTypeImpl methodTypeImpl) {
        methodTypeImpl.struct = methodTypeImpl.getType().struct;
    }

    public void visit(FormParam1 formParam) {
        if ("main".equalsIgnoreCase(currentMethod.getName())) {
            report_error("main funkcija mora biti bez parametara.", formParam);
            return;
        }
        Obj objExists = Tab.find(formParam.getParamName());
        if (objExists != Tab.noObj && objExists.getLevel() == level && objExists.getKind() == Obj.Var) {
            report_error("ime " + formParam.getParamName() + " je vec deklarisano u opsegu", formParam);
        } else {
            if (isArray) {
                Obj obj = Tab.insert(Obj.Var, formParam.getParamName(), new Struct(Struct.Array, formParam.getType().struct));
                obj.setLevel(level);
                obj.setFpPos(numOfFormalArgs);
                isArray = false;
            } else {
                Obj obj = Tab.insert(Obj.Var, formParam.getParamName(), formParam.getType().struct);
                obj.setLevel(level);
                obj.setFpPos(numOfFormalArgs);
            }
            numOfFormalArgs++;
        }
    }

    public void visit(ActParam actParam) {
        actParam.struct = actParam.getExpr1().struct;
        actParamTypes.add(actParam.struct);
    }

    public void visit(AssignOperation assignOperation) {
        Obj designator = ((DesignatorStatement1) assignOperation.getParent()).getDesignator().obj;
        int kind = designator.getKind();
        if (kind != Obj.Var && kind != Obj.Fld) {
            report_error("designator mora oznacavati promenljivu, element niza ili polje objekta", null);
        }

        Expr1 expr1 = assignOperation.getExpr1();
        Struct designatorNode = designator.getType();
        if (expr1 instanceof ExprImpl) {
            Expr expr = ((ExprImpl) expr1).getExpr();
            if (expr instanceof ExprSum) {
                Struct exprNode = expr.struct;
                if (!checkTypeCompatibility(designatorNode, exprNode)) {
                    report_error("nekompatibilni tipovi u dodeli vrednosti. ", assignOperation);
                } else if (exprNode.getKind() == Struct.Array && !isNewStmt(exprNode)) {
                    report_error("Niz se ne moze naci sa desne strane dodele vrednosti.", assignOperation);
                }
            } else if (expr instanceof SwitchYieldExpr) {
                YieldVisitor visitor = new YieldVisitor(this, designatorNode);
                expr.traverseTopDown(visitor);
            }
        } else if (!checkTypeCompatibility(designatorNode, expr1.struct)) {
            report_error("nekompatibilni tipovi u dodeli vrednosti! ", assignOperation);
        }
    }

    protected boolean isNewStmt(Struct node) {
        return node.getMembersTable().symbols().size() > 0;
    }

    protected boolean checkTypeCompatibility(Struct left, Struct right) {
        if (left.compatibleWith(right)) {
            return true;
        } else if (left.getKind() == Struct.Array && left.getKind() == right.getKind()
                && left.getElemType().getKind() == Struct.None) {
            return true;
        } else if (left.getKind() == Struct.Class) {
            Struct type = right.getElemType();
            while (type != null) {
                if (left.compatibleWith(type))
                    return true;
                type = type.getElemType();
            }
        }
        return false;
    }

    public void visit(FunctionCall functionCall) {
        Obj designator = ((DesignatorStatement1) functionCall.getParent()).getDesignator().obj;
        checkFormalAndActParsCompatibility(designator, functionCall);
    }

    public void visit(Increment increment) {
        Obj designator = ((DesignatorStatement1) increment.getParent()).getDesignator().obj;
        checkDesignatorIncDec(designator, increment.getParent());
    }

    public void visit(Decrement decrement) {
        Obj designator = ((DesignatorStatement1) decrement.getParent()).getDesignator().obj;
        checkDesignatorIncDec(designator, decrement.getParent());
    }

    protected void checkDesignatorIncDec(Obj designator, SyntaxNode syntaxNode) {
        int kind = designator.getKind();
        if (kind != Obj.Var && kind != Obj.Fld || designator.getType().getKind() == Struct.Array) {
            report_error("operand u izrazu mora oznacavati promenljivu, element niza ili polje objekta", syntaxNode);
        }
        Struct type = designator.getType();
        if (type != Tab.intType) {
            report_error("ocekivani tip: int", syntaxNode);
        }
    }

    public void visit(FactorDesignator factorDesignator) {
        factorDesignator.struct = factorDesignator.getDesignator().obj.getType();
        if (factorDesignator.getOptActParsInBrackets() instanceof ActParsInBrackets) {
            Obj designator = factorDesignator.getDesignator().obj;
            checkFormalAndActParsCompatibility(designator, factorDesignator);
        }
    }

    protected void checkFormalAndActParsCompatibility(Obj designator, SyntaxNode node) {
        if (designator.getKind() != Obj.Meth) {
            report_error(designator.getName() + " nije funkcija.", node);
            return;
        }

        int numberOfFormalArgs = designator.getFpPos() - designator.getLevel();
        if (numberOfFormalArgs != actParamTypes.size()) {
            report_error("greska pri pozivu funkcije, ocekivani broj parametara: " + numberOfFormalArgs
                    + ", stvarni broj parametara: " + actParamTypes.size(), node);
            actParamTypes.clear();
            return;
        }

        Collection<Obj> formalParams = designator.getLocalSymbols();
        int counter = 0;
        for (Obj param : formalParams) {
            if (counter == numberOfFormalArgs) break;
            if (counter == 0 && designator.getLevel() > 0) continue;
            counter++;

            Struct paramStruct = param.getType();
            Struct actualStruct = actParamTypes.get(param.getFpPos());
            if (!checkTypeCompatibility(paramStruct, actualStruct)) {
                report_error("formalni i stvarni argument na poziciji " + param.getFpPos()
                        + " nisu kompatibilni pri dodeli vrednosti.", node);
            }
        }
        actParamTypes.clear();
    }

    public void visit(FactorConst factorConst) {
        factorConst.struct = factorConst.getConstValue().struct;
    }

    public void visit(FactorNew factorNew) {
        Struct type = factorNew.getType().struct;
        if (factorNew.getOptExprInSquares() instanceof ExprInSquares) {
            factorNew.struct = new Struct(Struct.Array, type);
            HashTableDataStructure htds = new HashTableDataStructure();
            htds.insertKey(new Obj(1, "", type));
            factorNew.struct.setMembers(htds);
        } else {
            if (type.getKind() == Struct.Class) {
                factorNew.struct = type;
            } else {
                report_error("operand new operatora mora oznacavati klasu.", factorNew);
                factorNew.struct = Tab.noType;
            }
        }
    }

    public void visit(FactorExpr factorExpr) {
        factorExpr.struct = factorExpr.getExpr1().struct;
    }

    public void visit(FactorTerm factorTerm) {
        factorTerm.struct = factorTerm.getFactor().struct;
    }

    public void visit(TermImpl termImpl) {
        Struct t = termImpl.getTerm().struct;
        if (termImpl.getOptMinus().getClass() == MinusBegin.class && t != Tab.intType) {
            report_error("operand unarnog operatora mora biti tipa int.", termImpl);
            termImpl.struct = Tab.noType;
        } else {
            termImpl.struct = termImpl.getTerm().struct;
        }

    }

    public void visit(ExprSum expr) {
        expr.struct = expr.getSum().struct;
    }

    public void visit(DefaultStatement defaultStatement) {
        DefaultVisitor defaultVisitor = new DefaultVisitor(this);
        defaultStatement.traverseTopDown(defaultVisitor);
        if (!defaultVisitor.getStatus()) {
            report_error("default grana mora sadrzati yield naredbu.", defaultStatement);
        }
    }

    public void visit(YieldStatement yieldStatement) {
        SyntaxNode parent = yieldStatement.getParent();
        while (parent != null) {
            if (parent instanceof SwitchYieldExpr) {
                return;
            }
            parent = parent.getParent();
        }
        report_error("yield naredba se moze naci samo unutar switch-a sa povratnom vrednoscu.", yieldStatement.getParent());
    }

    public void visit(MulTerm mulTerm) {
        Struct t1 = mulTerm.getTerm().struct;
        Struct t2 = mulTerm.getFactor().struct;
        if (t1.equals(t2) && t1 == Tab.intType) {
            mulTerm.struct = t1;
        } else {
            report_error("nekompatibilni tipovi u izrazu za mnozenje ili nisu tipa int.", mulTerm);
            mulTerm.struct = Tab.noType;
        }
    }

    public void visit(SumImpl sum) {
        Struct t1 = sum.getSum().struct;
        Struct t2 = sum.getTerm().struct;
        if (t1.equals(t2) && t1 == Tab.intType) {
            sum.struct = t1;
        } else {
            report_error("nekompatibilni tipovi u izrazu za sabiranje ili nisu tipa int.", sum);
            sum.struct = Tab.noType;
        }
    }

    public void visit(BreakStatement breakStatement) {
        SyntaxNode parent = breakStatement.getParent();
        while (parent != null) {
            if (parent instanceof DoWhileStatement || parent instanceof SwitchStatement) {
                return;
            }
            parent = parent.getParent();
        }
        report_error("break se moze naci samo unutar do-while petlje ili switch naredbe", breakStatement.getParent());
    }

    public void visit(ContinueStatement continueStatement) {
        SyntaxNode parent = continueStatement.getParent();
        while (parent != null) {
            if (parent instanceof DoWhileStatement) {
                return;
            }
            parent = parent.getParent();
        }
        report_error("continue se moze naci samo unutar do-while petlje.", continueStatement.getParent());
    }

    public void visit(DesignatorValArray designatorValArray) {
        Expr1 expr = designatorValArray.getExpr1();
        if (expr.struct != Tab.intType) {
            report_error("indeks niza mora biti int.", designatorValArray);
        } else {
            designatorChainQueue.add(new DesignatorNode(null, "")); //empty string indicates array type
        }
    }

    public void visit(DesignatorVal designatorVal) {
        designatorChainQueue.add(new DesignatorNode(designatorVal,
                designatorVal.getDesignatorValName().getName()));
    }

    public void visit(SingleDesignator designator) {
        designatorChainQueue.add(new DesignatorNode(designator.getDesignatorFirst(),
                designator.getDesignatorFirst().getName()));
        designator.obj = checkDesignatorChainAndGetObj(designator);
        designatorChainQueue.clear();
    }

    public void visit(DesignatorDot designatorDot) {
        designatorDot.obj = checkDesignatorChainAndGetObj(designatorDot);
        designatorChainQueue.clear();
    }

    public void visit(DesignatorFirst designatorFirst) {
        SyntaxNode parent = designatorFirst.getParent();
        if (!(parent instanceof DesignatorFirstArr) && !(parent instanceof SingleDesignator))
            designatorChainQueue.add(new DesignatorNode(designatorFirst, designatorFirst.getName()));
    }

    public void visit(DesignatorArr designatorArr) {
        String firstElem = designatorChainQueue.peek().getName();
        if (!"".equals(firstElem)) {
            designatorArr.obj = Tab.noObj;
        } else {
            designatorArr.obj = checkDesignatorChainAndGetObj(designatorArr);
        }
        designatorChainQueue.clear();
    }

    public void visit(DesignatorFirstArr designator) {
        Expr1 expr = designator.getExpr1();
        if (expr.struct != Tab.intType) {
            report_error("indeks niza mora biti int.", designator);
            designatorChainQueue.add(new DesignatorNode(null, "Error"));
        } else {
            designatorChainQueue.add(new DesignatorNode(null, ""));
            designatorChainQueue.add(new DesignatorNode(designator.getDesignatorFirst(), designator.getDesignatorFirst().getName()));
        }
    }

    public void visit(StartOfDesignatorIndex start) {
        designatorChainQueueStack.push(designatorChainQueue);
        designatorChainQueue = new ArrayDeque<>();
    }

    public void visit(EndOfDesignatorIndex end) {
        designatorChainQueue = designatorChainQueueStack.pop();
    }

    protected Obj checkDesignatorChainAndGetObj(SyntaxNode syntaxNode) {
        boolean arrayNode = false;
        DesignatorNode designatorNode = designatorChainQueue.poll();
        String firstElem = designatorNode.getName();
        Obj currentObj;
        Struct currentStruct;
        if ("".equals(firstElem)) {
            designatorNode = designatorChainQueue.poll();
            firstElem = designatorNode.getName();
            currentObj = Tab.find(firstElem);
            currentStruct = currentObj.getType().getElemType();
            arrayNode = true;
            report_info_detected_symbol(currentObj, Obj.Elem, syntaxNode.getLine());
        } else {
            currentObj = findFirstElem(firstElem);
            currentStruct = currentObj.getType();
            report_info_detected_symbol(currentObj, currentObj.getKind(), syntaxNode.getLine());
        }

        if (currentObj == Tab.noObj) {
            report_error("ime " + firstElem + " nije deklarisano! ", syntaxNode);
            return Tab.noObj;
        } else {
            designatorNode.setObj(currentObj);
        }

        if (!checkNode(currentObj, arrayNode)) {
            if (!designatorChainQueue.isEmpty() || (designatorChainQueue.isEmpty() && arrayNode)) {
                report_error("nedozvoljeno koriscenje podatka tipa niza.", syntaxNode);
                return Tab.noObj;
            }
        }
        arrayNode = designatorChainQueue.isEmpty() ? arrayNode : false;
        while (!designatorChainQueue.isEmpty()) {
            designatorNode = designatorChainQueue.poll();
            String name = designatorNode.getName();
            if ("".equals(name)) {
                arrayNode = true;
            } else {
                SymbolDataStructure membersTable;
                if ("this".equals(firstElem)) {
                    membersTable = Tab.currentScope().getOuter().getLocals();
                } else {
                    membersTable = currentStruct.getMembersTable();
                }
                currentObj = membersTable.searchKey(name);
                if (currentObj != null) {
                    if (!checkNode(currentObj, arrayNode)
                            && (!designatorChainQueue.isEmpty()
                            || (designatorChainQueue.isEmpty() && arrayNode))) {
                        report_error("nedozvoljeno koriscenje podatka tipa niza.", syntaxNode);
                        currentObj = null;
                        break;
                    } else {
                        if (arrayNode) {
                            currentStruct = currentObj.getType().getElemType();
                            arrayNode = designatorChainQueue.isEmpty() ? arrayNode : false;
                            report_info_detected_symbol(currentObj, Obj.Elem, syntaxNode.getLine());
                        } else {
                            currentStruct = currentObj.getType();
                            report_info_detected_symbol(currentObj, currentObj.getKind(), syntaxNode.getLine());
                        }
                        designatorNode.setObj(currentObj);
                    }

                } else {
                    report_error("polje " + name + " ne postoji.", syntaxNode);
                    break;
                }
            }
        }
        if (currentObj == null) {
            return Tab.noObj;
        } else {
            Obj ret = new Obj(currentObj.getKind(), currentObj.getName(),
                    arrayNode ? currentObj.getType().getElemType() : currentObj.getType());
            ret.setLevel(currentObj.getLevel());
            ret.setFpPos(currentObj.getFpPos());
            if (currentObj.getKind() == Obj.Meth)
                ret.setLocals(convertToSymbolDataStructure(currentObj.getLocalSymbols()));
            return ret;
        }
    }

    protected SymbolDataStructure convertToSymbolDataStructure(Collection<Obj> localSymbols) {
        SymbolDataStructure dataStructure = new HashTableDataStructure();
        for (Obj obj : localSymbols) {
            dataStructure.insertKey(obj);
        }
        return dataStructure;
    }

    protected Obj findFirstElem(String firstElem) {
        Obj obj = Tab.find(firstElem);
        if (obj != Tab.noObj) return obj;

        if (currentClass != null) {
            Struct baseClass = currentClass.getType().getElemType();
            if (baseClass != null) {
                obj = baseClass.getMembersTable().searchKey(firstElem);
                if (obj != null && obj.getKind() == Obj.Meth) {
                    return obj;
                }
            }
        }
        return Tab.noObj;
    }


    public boolean checkNode(Obj obj, boolean isArray) {
        if ((isArray && obj.getType().getKind() != Struct.Array) ||
                (!isArray && obj.getType().getKind() == Struct.Array)) {
            return false;
        }
        return true;
    }

    public void visit(RelopCondFact relopCondFact) {
        Struct expr1 = relopCondFact.getExpr1().struct;
        Struct expr2 = relopCondFact.getExpr11().struct;
        relopCondFact.struct = boolType;
        if (!expr1.compatibleWith(expr2)) {
            report_error("izrazi koji se porede nisu kompatibinli.", relopCondFact.getParent().getParent().getParent());
            relopCondFact.struct = Tab.noType;
        } else {
            if (expr1.getKind() == Struct.Array || expr1.getKind() == Struct.Class) {
                Relop relop = relopCondFact.getRelop();
                if (!(relop instanceof RelopEq || relop instanceof RelopNeq)) {
                    report_error("za promenljive tipa Class ili Array dozvoljeno je " +
                            "samo poredjenje na jednakost ili nejednakost.", relopCondFact.getParent().getParent().getParent().getParent());
                    relopCondFact.struct = Tab.noType;
                }
            }
        }
    }

    public void visit(CondFactExpr condFactExpr) {
        Expr1 expr = condFactExpr.getExpr1();
        if (!expr.struct.equals(boolType)) {
            report_error("uslov nije tipa bool.", condFactExpr);
            condFactExpr.struct = Tab.noType;
        } else {
            condFactExpr.struct = boolType;
        }
    }

    public void visit(SimpleCondTerm condTerm) {
        condTerm.struct = condTerm.getCondFact().struct;
    }

    public void visit(AndConditionTerm condTerm) {
        if (!condTerm.getCondTerm().struct.equals(boolType)
                || !condTerm.getCondFact().struct.equals(boolType)) {
            condTerm.struct = Tab.noType;
        } else {
            condTerm.struct = boolType;
        }
    }

    public void visit(SimpleCondition condition) {
        condition.struct = condition.getCondTerm().struct;
    }

    public void visit(OrCondtion condition) {
        if (!condition.getCondition().struct.equals(boolType)
                || !condition.getCondTerm().struct.equals(boolType)) {
            condition.struct = Tab.noType;
        } else {
            condition.struct = boolType;
        }
    }

    public void visit(IfElseStatement ifStatement) {
        checkCondition(ifStatement.getCondition());
    }

    public void visit(IfThenStatement ifStatement) {
        checkCondition(ifStatement.getCondition());
    }

    public void visit(DoWhileStatement doWhileStatement) {
        checkCondition(doWhileStatement.getCondition());
    }

    protected void checkCondition(Condition condition) {
        if (condition.struct == null || !condition.struct.equals(boolType)) {
            report_error("uslov unutar naredbe nije tipa bool", condition);
        }
    }

    public void visit(SwitchStatement switchStatement) {
        if (!switchStatement.getExpr1().struct.equals(Tab.intType)) {
            report_error("Izraz unutar SWITCH naredbe mora biti celobrojnog tipa", switchStatement);
        }
        caseList.clear();
    }

    public void visit(CaseStatement caseStatement) {
        boolean ind = caseList.add(caseStatement.getNumConst());
        if (!ind) {
            report_error("vec postoji case grana sa vrednoscu " + caseStatement.getNumConst() + ".", caseStatement);
        }
    }

    public void visit(ExprInSquares exprInSquares) {
        Struct type = exprInSquares.getExpr1().struct;
        if (!Tab.intType.equals(type)) {
            report_error("izraz unutar uglastih zagrada nije tipa int.", exprInSquares);
        }
    }

    public void visit(ExprImpl exprImpl) {
        exprImpl.struct = exprImpl.getExpr().struct;
    }

    public void visit(TernaryOperator ternaryOperator) {
        Expr condition = ternaryOperator.getExpr();
        if (!condition.struct.equals(boolType)) {
            report_error("uslov u ternarnom operatoru nije tipa bool.", ternaryOperator.getParent());
        }

        Struct op1 = ternaryOperator.getExpr1().struct;
        Struct op2 = ternaryOperator.getExpr2().struct;
        if (!checkTypeCompatibility(op1, op2)) {
            report_error("izrazi u ternarnom operatoru nisu kompatibilni", ternaryOperator.getParent());
            ternaryOperator.struct = Tab.noType;
        } else {
            ternaryOperator.struct = op1;
        }
    }
}
