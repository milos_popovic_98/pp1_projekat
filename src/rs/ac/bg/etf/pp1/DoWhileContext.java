package rs.ac.bg.etf.pp1;

import java.util.ArrayList;
import java.util.List;

public class DoWhileContext extends BranchContext {
    private List<Integer> branchesAfter = new ArrayList<>();
    private List<Integer> branchesOn = new ArrayList<>();
    private List<Integer> continueBranches = new ArrayList<>();
    private List<Integer> thenBranches = new ArrayList<>();
    int doLabelAdr;

    @Override
    public void addBranchOn(int adr) {
        branchesOn.add(adr);
    }

    @Override
    public void addBranchAfter(int adr) {
        branchesAfter.add(adr);
    }

    @Override
    public void addBranchThen(int adr) {
        thenBranches.add(adr);
    }

    @Override
    public List<Integer> getBranchesOn() {
        return branchesOn;
    }

    @Override
    public List<Integer> getBranchesAfter() {
        return branchesAfter;
    }

    @Override
    public List<Integer> getBranchesThen() {
        return thenBranches;
    }

    public int getDoLabelAdr() {
        return doLabelAdr;
    }

    public void setDoLabelAdr(int doLabelAdr) {
        this.doLabelAdr = doLabelAdr;
    }

    public void addContinueBranch(int adr) {
        continueBranches.add(adr);
    }

    public List<Integer> getContinueBranches() {
        return continueBranches;
    }

}
